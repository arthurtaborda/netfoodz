package com.netfoodz.common.infrastructure.jooq.converter;

import com.netfoodz.common.domain.model.EmailAddress;
import org.jooq.Converter;

public class EmailAddressConverter implements Converter<String, EmailAddress> {

	@Override
	public EmailAddress from(String databaseObject) {
		return new EmailAddress(databaseObject);
	}

	@Override
	public String to(EmailAddress userObject) {
		return userObject.getStringValue();
	}

	@Override
	public Class<String> fromType() {
		return String.class;
	}

	@Override
	public Class<EmailAddress> toType() {
		return EmailAddress.class;
	}
}
