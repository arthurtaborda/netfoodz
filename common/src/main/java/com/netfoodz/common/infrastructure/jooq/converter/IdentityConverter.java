package com.netfoodz.common.infrastructure.jooq.converter;

import com.netfoodz.common.domain.model.Identity;
import org.jooq.Converter;

public class IdentityConverter implements Converter<String, Identity> {

	@Override
	public Identity from(String databaseObject) {
		return Identity.of(databaseObject);
	}

	@Override
	public String to(Identity userObject) {
		return userObject.toString();
	}

	@Override
	public Class<String> fromType() {
		return String.class;
	}

	@Override
	public Class<Identity> toType() {
		return Identity.class;
	}
}
