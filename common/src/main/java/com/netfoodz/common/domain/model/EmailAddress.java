package com.netfoodz.common.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;
import java.util.regex.Pattern;

	@EqualsAndHashCode(of = "stringValue")
public final class EmailAddress implements Serializable {

	@Getter
	private String stringValue;

	public EmailAddress(String stringValue) {
		boolean isValid = Pattern.matches("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", stringValue);
		if (stringValue == null) {
			throw new IllegalArgumentException("The email stringValue is required");
		} else if (stringValue.length() < 5 || stringValue.length() > 100) {
			throw new IllegalArgumentException("Email stringValue must be 100 characters or less");
		} else if (!isValid) {
			throw new IllegalArgumentException("Email stringValue format is invalid");
		}

		this.stringValue = stringValue;
	}
}
