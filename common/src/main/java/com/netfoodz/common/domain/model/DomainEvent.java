package com.netfoodz.common.domain.model;

import java.util.Date;

public interface DomainEvent {

    Date occurredOn();
}
