package com.netfoodz.common.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.UUID;

@EqualsAndHashCode
@AllArgsConstructor
public class Identity implements Serializable {

	private String id;

	public static Identity generate() {
		return new Identity(UUID.randomUUID().toString());
	}

	public static Identity of(String id) {
		if(id == null || id.isEmpty()) return null;
		return new Identity(id);
	}

	@Override
	public String toString() {
		return id;
	}
}
