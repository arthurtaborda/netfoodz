package com.netfoodz.common.domain.util;

import java.text.SimpleDateFormat;

public class Constants {

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
