package com.netfoodz.rest.controller;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.application.CheckoutApplicationService;
import com.netfoodz.store.checkout.application.OrderDTO;
import com.netfoodz.store.customer.application.CartApplicationService;
import com.netfoodz.store.customer.application.CartDTO;
import com.netfoodz.store.customer.application.command.AddItemToCartCommand;
import com.netfoodz.store.customer.application.command.RemoveItemFromCartCommand;
import com.netfoodz.store.customer.domain.exception.CartAlreadyEmptyException;
import com.netfoodz.store.customer.domain.exception.ProductNotInCartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cart")
public class CartController {

	private CartApplicationService cartService;
	private CheckoutApplicationService checkoutApplicationService;

	@Autowired
	public CartController(CartApplicationService cartService, CheckoutApplicationService checkoutApplicationService) {
		this.cartService = cartService;
		this.checkoutApplicationService = checkoutApplicationService;
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
	public ResponseEntity<CartDTO> get(@PathVariable("cartId") String cartId) {

		CartDTO dto = cartService.getCart(Identity.of(cartId));

		return ResponseEntity.ok().body(dto);
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{cartId}/product/{productId}", method = RequestMethod.POST)
	public ResponseEntity<Void> addProduct(@PathVariable("cartId") String cartId,
										   @PathVariable("productId") String productId,
										   @RequestParam(defaultValue = "1") Integer quantity,
										   @RequestParam(required = false) String observation) {

		AddItemToCartCommand command = new AddItemToCartCommand(Identity.of(cartId), Identity.of(productId));
		command.setQuantity(quantity);
		command.setObservation(observation);
		cartService.addItemToCart(command);

		return ResponseEntity.ok().build();
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{cartId}/product/{productId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> removeProduct(@PathVariable("cartId") String cartId,
											  @PathVariable("productId") String productId) throws ProductNotInCartException {
		RemoveItemFromCartCommand command = new RemoveItemFromCartCommand(Identity.of(cartId), Identity.of(productId));
		cartService.removeItemFromCart(command);

		return ResponseEntity.ok().build();
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{cartId}/checkout", method = RequestMethod.POST)
	public ResponseEntity<OrderDTO> checkout(@PathVariable("cartId") String cartId) throws ProductNotInCartException {
		OrderDTO order = checkoutApplicationService.checkout(Identity.of(cartId));

		return ResponseEntity.ok(order);
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{cartId}/clear", method = RequestMethod.POST)
	public ResponseEntity clear(@PathVariable("cartId") String cartId) throws ProductNotInCartException, CartAlreadyEmptyException {
		cartService.clearCart(Identity.of(cartId));

		return ResponseEntity.ok().build();
	}
}
