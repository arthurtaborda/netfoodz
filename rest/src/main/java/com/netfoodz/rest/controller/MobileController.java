package com.netfoodz.rest.controller;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.mobile.application.MobileApplicationService;
import com.netfoodz.store.mobile.domain.model.Location;
import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/mobile")
public class MobileController {

	private MobileApplicationService mobileApplicationService;

	@Autowired
	public MobileController(MobileApplicationService mobileApplicationService) {
		this.mobileApplicationService = mobileApplicationService;
	}

	@RequestMapping(value = "/location/{deviceId}", method = RequestMethod.POST)
	public ResponseEntity removeProductFromCart(@PathVariable("deviceId") String deviceId,
												@RequestParam("lat") Double lat,
												@RequestParam("lng") Double lng,
												@RequestParam("platform") String platform) {
		MobilePlatform plat = null;
		try {
			plat = MobilePlatform.valueOf(platform.toUpperCase());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Invalid platform. It must be apple or android");
		}

		mobileApplicationService.updateLocation(Identity.of(deviceId), new Location(lat, lng), plat);

		return ResponseEntity.ok().build();
	}
}
