package com.netfoodz.rest.controller;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.application.OrderApplicationService;
import com.netfoodz.store.checkout.application.OrderDTO;
import com.netfoodz.store.checkout.application.PaymentApplicationService;
import com.netfoodz.store.checkout.domain.exception.PaymentGatewayException;
import com.netfoodz.store.checkout.domain.model.CreditCard;
import com.netfoodz.store.checkout.domain.model.CreditCardNumber;
import com.netfoodz.store.checkout.domain.model.OrderState;
import com.netfoodz.store.mobile.application.MobileApplicationService;
import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.time.Year;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/order")
public class OrderController {

	private OrderApplicationService orderService;
	private MobileApplicationService mobileApplicationService;
	private PaymentApplicationService paymentApplicationService;

	@Autowired
	public OrderController(OrderApplicationService orderService, MobileApplicationService mobileApplicationService, PaymentApplicationService paymentApplicationService) {
		this.orderService = orderService;
		this.mobileApplicationService = mobileApplicationService;
		this.paymentApplicationService = paymentApplicationService;
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
	public ResponseEntity<OrderDTO> get(@PathVariable("orderId") String orderId) {
		OrderDTO dto = orderService.get(Identity.of(orderId));

		return ResponseEntity.ok(dto);
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{orderId}/inProcess", method = RequestMethod.POST)
	public ResponseEntity<Void> orderInProcess(@PathVariable("orderId") String orderId,
											   @RequestParam("orderCode") String orderCode,
											   @RequestParam(value = "deviceId", required = false) String deviceId,
											   @RequestParam(value = "platform", required = false) String platform) {
		orderService.setInProcess(Identity.of(orderId));

		mobileApplicationService.sendOrderInProgressNotification(Identity.of(deviceId),
				MobilePlatform.valueOf(platform), orderCode);

		return ResponseEntity.ok().build();
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{orderId}/isPaid", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Boolean>> isOrderPaid(@PathVariable("orderId") String orderId) {
		OrderDTO dto = orderService.get(Identity.of(orderId));

		Map<String, Boolean> result = new HashMap<>();
		if(dto == null)
			result.put("paid", false);
		else
			result.put("paid", OrderState.PAID.toString().equals(dto.state));


		return ResponseEntity.ok(result);
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{orderId}/pay/creditCard", method = RequestMethod.POST)
	public ResponseEntity payWithCreditCard(@PathVariable("orderId") String orderId,
												 @RequestParam("name") String name,
												 @RequestParam("number") String number,
												 @RequestParam("expirationMonth") Integer expirationMonth,
												 @RequestParam("expirationYear") Integer expirationYear,
												 @RequestParam("cvv") Integer cvv) {

		CreditCardNumber creditCardNumber = new CreditCardNumber(number);
		CreditCard creditCard = new CreditCard(creditCardNumber, name, Month.of(expirationMonth), Year.of
				(expirationYear), cvv);

		try {
			paymentApplicationService.pay(Identity.of(orderId), creditCard);
		} catch (PaymentGatewayException e) {
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("An error ocurred on the payment gateway");
		}

		return ResponseEntity.ok().build();
	}
}
