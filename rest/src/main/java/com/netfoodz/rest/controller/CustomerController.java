package com.netfoodz.rest.controller;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.authentication.application.SessionService;
import com.netfoodz.store.checkout.application.OrderApplicationService;
import com.netfoodz.store.checkout.application.OrderDTO;
import com.netfoodz.store.customer.application.CartApplicationService;
import com.netfoodz.store.customer.application.CustomerApplicationService;
import com.netfoodz.store.customer.application.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

	private SessionService sessionService;
	private OrderApplicationService orderService;
	private CartApplicationService cartService;
	private CustomerApplicationService customerApplicationService;

	@Autowired
	public CustomerController(SessionService sessionService, OrderApplicationService orderService, CartApplicationService cartService, CustomerApplicationService customerApplicationService) {
		this.sessionService = sessionService;
		this.orderService = orderService;
		this.cartService = cartService;
		this.customerApplicationService = customerApplicationService;
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public ResponseEntity<CustomerDTO> getMe() {

		CustomerDTO dto = customerApplicationService.getCustomer(sessionService.getCustomerId());

		return ResponseEntity.ok(dto);
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{customerId}/carts", method = RequestMethod.GET)
	public ResponseEntity getCartsIds(@PathVariable("customerId") String customerId) {

		List<Identity> ids = cartService.getCartsIds(Identity.of(customerId));

		if (ids == null || ids.isEmpty())
			return ResponseEntity.noContent().build();

		Map<String, List<String>> result = new HashMap<>();
		result.put("id", ids.stream().map(Identity::toString).collect(Collectors.toList()));

		return ResponseEntity.ok().body(result);
	}


	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{customerId}/orders", method = RequestMethod.GET)
	public ResponseEntity getOrdersIds(@PathVariable("customerId") String customerId) {

		List<OrderDTO> orders = orderService.getOrders(Identity.of(customerId));

		if (orders == null || orders.isEmpty())
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok().body(orders);
	}
}
