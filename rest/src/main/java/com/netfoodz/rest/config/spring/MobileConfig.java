package com.netfoodz.rest.config.spring;

import com.google.android.gcm.server.Sender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MobileConfig {

	@Value("${application.gcm.key}")
	private String gcmKey;

	@Bean
	public Sender gcmSender() {
		return new Sender(gcmKey);
	}
}
