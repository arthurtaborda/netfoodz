package com.netfoodz.rest.config.spring;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class JooqConfig {

	@Primary
	@Bean(name = "dataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

//	@Value("${spring.datasource.driverClassName}")
//	private String driverClassName;
//	@Value("${spring.datasource.url}")
//	private String url;
//	@Value("${spring.datasource.username}")
//	private String username;
//	@Value("${spring.datasource.password}")
//	private String password;
//
//	@Bean
//	public DataSource dataSource() {
//		HikariDataSource dataSource = new HikariDataSource();
//		dataSource.setDriverClassName(driverClassName);
//		dataSource.setJdbcUrl(url);
//		dataSource.setUsername(username);
//		dataSource.setPassword(password);
//		return dataSource;
//	}

	@Bean
	public DataSourceConnectionProvider dataSourceConnectionProvider() {
		return new DataSourceConnectionProvider(dataSource());
	}

	@Bean
	public DefaultConfiguration defaultConfiguration() {
		DefaultConfiguration defaultConfiguration = new DefaultConfiguration();
		defaultConfiguration.setConnectionProvider(dataSourceConnectionProvider());
		defaultConfiguration.setSQLDialect(SQLDialect.MYSQL);
		return defaultConfiguration;
	}

	@Bean
	public DSLContext dslContext() {
		return new DefaultDSLContext(defaultConfiguration());
	}
}
