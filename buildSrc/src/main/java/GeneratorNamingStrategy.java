import org.jooq.util.DefaultGeneratorStrategy;
import org.jooq.util.Definition;
import org.jooq.util.TableDefinition;

public class GeneratorNamingStrategy extends DefaultGeneratorStrategy {
	@Override
	public String getJavaClassName(final Definition definition, final Mode mode) {
//		if(mode == Mode.POJO)
//			return "JQ" + super.getJavaClassName(definition, mode);
		if (mode == Mode.DEFAULT && definition instanceof TableDefinition)
			return "TBL" + super.getJavaClassName(definition, mode);
		if (mode == Mode.DAO || mode == Mode.POJO || mode ==  Mode.RECORD)
			return "JQ" + super.getJavaClassName(definition, mode);
		return super.getJavaClassName(definition, mode);
	}
}
