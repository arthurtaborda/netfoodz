/**
 * This class is generated by jOOQ
 */
package com.netfoodz.jooq.tables;


import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.common.infrastructure.jooq.converter.IdentityConverter;
import com.netfoodz.jooq.Keys;
import com.netfoodz.jooq.NetfoodzDev;
import com.netfoodz.jooq.tables.records.JQNotificationRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TBLNotification extends TableImpl<JQNotificationRecord> {

    private static final long serialVersionUID = -343331461;

    /**
     * The reference instance of <code>netfoodz_dev.notification</code>
     */
    public static final TBLNotification NOTIFICATION = new TBLNotification();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<JQNotificationRecord> getRecordType() {
        return JQNotificationRecord.class;
    }

    /**
     * The column <code>netfoodz_dev.notification.id</code>.
     */
    public final TableField<JQNotificationRecord, Identity> ID = createField("id", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false), this, "", new IdentityConverter());

    /**
     * The column <code>netfoodz_dev.notification.device_id</code>.
     */
    public final TableField<JQNotificationRecord, String> DEVICE_ID = createField("device_id", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false), this, "");

    /**
     * The column <code>netfoodz_dev.notification.notification_id</code>.
     */
    public final TableField<JQNotificationRecord, String> NOTIFICATION_ID = createField("notification_id", org.jooq.impl.SQLDataType.VARCHAR.length(255), this, "");

    /**
     * The column <code>netfoodz_dev.notification.notification_group_id</code>.
     */
    public final TableField<JQNotificationRecord, String> NOTIFICATION_GROUP_ID = createField("notification_group_id", org.jooq.impl.SQLDataType.VARCHAR.length(255), this, "");

    /**
     * The column <code>netfoodz_dev.notification.error_message</code>.
     */
    public final TableField<JQNotificationRecord, String> ERROR_MESSAGE = createField("error_message", org.jooq.impl.SQLDataType.VARCHAR.length(255), this, "");

    /**
     * The column <code>netfoodz_dev.notification.device_deactivated</code>.
     */
    public final TableField<JQNotificationRecord, Byte> DEVICE_DEACTIVATED = createField("device_deactivated", org.jooq.impl.SQLDataType.TINYINT.nullable(false).defaultValue(org.jooq.impl.DSL.inline("0", org.jooq.impl.SQLDataType.TINYINT)), this, "");

    /**
     * The column <code>netfoodz_dev.notification.error_codename</code>.
     */
    public final TableField<JQNotificationRecord, String> ERROR_CODENAME = createField("error_codename", org.jooq.impl.SQLDataType.VARCHAR.length(255), this, "");

    /**
     * The column <code>netfoodz_dev.notification.status</code>.
     */
    public final TableField<JQNotificationRecord, String> STATUS = createField("status", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false).defaultValue(org.jooq.impl.DSL.inline("SUCCESS", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>netfoodz_dev.notification.platform</code>.
     */
    public final TableField<JQNotificationRecord, String> PLATFORM = createField("platform", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false).defaultValue(org.jooq.impl.DSL.inline("ANDROID", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>netfoodz_dev.notification.stack_trace</code>.
     */
    public final TableField<JQNotificationRecord, String> STACK_TRACE = createField("stack_trace", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * Create a <code>netfoodz_dev.notification</code> table reference
     */
    public TBLNotification() {
        this("notification", null);
    }

    /**
     * Create an aliased <code>netfoodz_dev.notification</code> table reference
     */
    public TBLNotification(String alias) {
        this(alias, NOTIFICATION);
    }

    private TBLNotification(String alias, Table<JQNotificationRecord> aliased) {
        this(alias, aliased, null);
    }

    private TBLNotification(String alias, Table<JQNotificationRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return NetfoodzDev.NETFOODZ_DEV;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<JQNotificationRecord> getPrimaryKey() {
        return Keys.KEY_NOTIFICATION_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<JQNotificationRecord>> getKeys() {
        return Arrays.<UniqueKey<JQNotificationRecord>>asList(Keys.KEY_NOTIFICATION_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<JQNotificationRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<JQNotificationRecord, ?>>asList(Keys.FK_NOTIFICATION_DEVICE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TBLNotification as(String alias) {
        return new TBLNotification(alias, this);
    }

    /**
     * Rename this table
     */
    public TBLNotification rename(String name) {
        return new TBLNotification(name, null);
    }
}
