/**
 * This class is generated by jOOQ
 */
package com.netfoodz.jooq.tables.daos;


import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.TBLOrderline;
import com.netfoodz.jooq.tables.pojos.JQOrderline;
import com.netfoodz.jooq.tables.records.JQOrderlineRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Repository
public class JQOrderlineDao extends DAOImpl<JQOrderlineRecord, JQOrderline, Identity> {

    /**
     * Create a new JQOrderlineDao without any configuration
     */
    public JQOrderlineDao() {
        super(TBLOrderline.ORDERLINE, JQOrderline.class);
    }

    /**
     * Create a new JQOrderlineDao with an attached configuration
     */
    @Autowired
    public JQOrderlineDao(Configuration configuration) {
        super(TBLOrderline.ORDERLINE, JQOrderline.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Identity getId(JQOrderline object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<JQOrderline> fetchById(Identity... values) {
        return fetch(TBLOrderline.ORDERLINE.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public JQOrderline fetchOneById(Identity value) {
        return fetchOne(TBLOrderline.ORDERLINE.ID, value);
    }

    /**
     * Fetch records that have <code>order_id IN (values)</code>
     */
    public List<JQOrderline> fetchByOrderId(String... values) {
        return fetch(TBLOrderline.ORDERLINE.ORDER_ID, values);
    }

    /**
     * Fetch records that have <code>product_id IN (values)</code>
     */
    public List<JQOrderline> fetchByProductId(String... values) {
        return fetch(TBLOrderline.ORDERLINE.PRODUCT_ID, values);
    }

    /**
     * Fetch records that have <code>price IN (values)</code>
     */
    public List<JQOrderline> fetchByPrice(Double... values) {
        return fetch(TBLOrderline.ORDERLINE.PRICE, values);
    }

    /**
     * Fetch records that have <code>quantity IN (values)</code>
     */
    public List<JQOrderline> fetchByQuantity(Integer... values) {
        return fetch(TBLOrderline.ORDERLINE.QUANTITY, values);
    }

    /**
     * Fetch records that have <code>observation IN (values)</code>
     */
    public List<JQOrderline> fetchByObservation(String... values) {
        return fetch(TBLOrderline.ORDERLINE.OBSERVATION, values);
    }
}
