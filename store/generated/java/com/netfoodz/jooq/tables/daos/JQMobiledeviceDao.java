/**
 * This class is generated by jOOQ
 */
package com.netfoodz.jooq.tables.daos;


import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.TBLMobiledevice;
import com.netfoodz.jooq.tables.pojos.JQMobiledevice;
import com.netfoodz.jooq.tables.records.JQMobiledeviceRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Repository
public class JQMobiledeviceDao extends DAOImpl<JQMobiledeviceRecord, JQMobiledevice, Identity> {

    /**
     * Create a new JQMobiledeviceDao without any configuration
     */
    public JQMobiledeviceDao() {
        super(TBLMobiledevice.MOBILEDEVICE, JQMobiledevice.class);
    }

    /**
     * Create a new JQMobiledeviceDao with an attached configuration
     */
    @Autowired
    public JQMobiledeviceDao(Configuration configuration) {
        super(TBLMobiledevice.MOBILEDEVICE, JQMobiledevice.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Identity getId(JQMobiledevice object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<JQMobiledevice> fetchById(Identity... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public JQMobiledevice fetchOneById(Identity value) {
        return fetchOne(TBLMobiledevice.MOBILEDEVICE.ID, value);
    }

    /**
     * Fetch records that have <code>active IN (values)</code>
     */
    public List<JQMobiledevice> fetchByActive(Byte... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.ACTIVE, values);
    }

    /**
     * Fetch records that have <code>lat IN (values)</code>
     */
    public List<JQMobiledevice> fetchByLat(Double... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.LAT, values);
    }

    /**
     * Fetch records that have <code>lng IN (values)</code>
     */
    public List<JQMobiledevice> fetchByLng(Double... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.LNG, values);
    }

    /**
     * Fetch records that have <code>platform IN (values)</code>
     */
    public List<JQMobiledevice> fetchByPlatform(String... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.PLATFORM, values);
    }

    /**
     * Fetch records that have <code>current_user_id IN (values)</code>
     */
    public List<JQMobiledevice> fetchByCurrentUserId(String... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.CURRENT_USER_ID, values);
    }

    /**
     * Fetch records that have <code>previous_user_id IN (values)</code>
     */
    public List<JQMobiledevice> fetchByPreviousUserId(String... values) {
        return fetch(TBLMobiledevice.MOBILEDEVICE.PREVIOUS_USER_ID, values);
    }
}
