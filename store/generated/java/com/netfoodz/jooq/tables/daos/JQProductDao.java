/**
 * This class is generated by jOOQ
 */
package com.netfoodz.jooq.tables.daos;


import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.TBLProduct;
import com.netfoodz.jooq.tables.pojos.JQProduct;
import com.netfoodz.jooq.tables.records.JQProductRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Repository
public class JQProductDao extends DAOImpl<JQProductRecord, JQProduct, Identity> {

    /**
     * Create a new JQProductDao without any configuration
     */
    public JQProductDao() {
        super(TBLProduct.PRODUCT, JQProduct.class);
    }

    /**
     * Create a new JQProductDao with an attached configuration
     */
    @Autowired
    public JQProductDao(Configuration configuration) {
        super(TBLProduct.PRODUCT, JQProduct.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Identity getId(JQProduct object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<JQProduct> fetchById(Identity... values) {
        return fetch(TBLProduct.PRODUCT.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public JQProduct fetchOneById(Identity value) {
        return fetchOne(TBLProduct.PRODUCT.ID, value);
    }

    /**
     * Fetch records that have <code>description IN (values)</code>
     */
    public List<JQProduct> fetchByDescription(String... values) {
        return fetch(TBLProduct.PRODUCT.DESCRIPTION, values);
    }

    /**
     * Fetch records that have <code>in_catalog IN (values)</code>
     */
    public List<JQProduct> fetchByInCatalog(Byte... values) {
        return fetch(TBLProduct.PRODUCT.IN_CATALOG, values);
    }

    /**
     * Fetch records that have <code>name IN (values)</code>
     */
    public List<JQProduct> fetchByName(String... values) {
        return fetch(TBLProduct.PRODUCT.NAME, values);
    }

    /**
     * Fetch records that have <code>price IN (values)</code>
     */
    public List<JQProduct> fetchByPrice(Double... values) {
        return fetch(TBLProduct.PRODUCT.PRICE, values);
    }
}
