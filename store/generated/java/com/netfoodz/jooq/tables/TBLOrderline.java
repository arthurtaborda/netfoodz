/**
 * This class is generated by jOOQ
 */
package com.netfoodz.jooq.tables;


import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.common.infrastructure.jooq.converter.IdentityConverter;
import com.netfoodz.jooq.Keys;
import com.netfoodz.jooq.NetfoodzDev;
import com.netfoodz.jooq.tables.records.JQOrderlineRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TBLOrderline extends TableImpl<JQOrderlineRecord> {

    private static final long serialVersionUID = 1061809497;

    /**
     * The reference instance of <code>netfoodz_dev.orderline</code>
     */
    public static final TBLOrderline ORDERLINE = new TBLOrderline();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<JQOrderlineRecord> getRecordType() {
        return JQOrderlineRecord.class;
    }

    /**
     * The column <code>netfoodz_dev.orderline.id</code>.
     */
    public final TableField<JQOrderlineRecord, Identity> ID = createField("id", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false), this, "", new IdentityConverter());

    /**
     * The column <code>netfoodz_dev.orderline.order_id</code>.
     */
    public final TableField<JQOrderlineRecord, String> ORDER_ID = createField("order_id", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false), this, "");

    /**
     * The column <code>netfoodz_dev.orderline.product_id</code>.
     */
    public final TableField<JQOrderlineRecord, String> PRODUCT_ID = createField("product_id", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false), this, "");

    /**
     * The column <code>netfoodz_dev.orderline.price</code>.
     */
    public final TableField<JQOrderlineRecord, Double> PRICE = createField("price", org.jooq.impl.SQLDataType.DOUBLE.nullable(false), this, "");

    /**
     * The column <code>netfoodz_dev.orderline.quantity</code>.
     */
    public final TableField<JQOrderlineRecord, Integer> QUANTITY = createField("quantity", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.inline("0", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>netfoodz_dev.orderline.observation</code>.
     */
    public final TableField<JQOrderlineRecord, String> OBSERVATION = createField("observation", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * Create a <code>netfoodz_dev.orderline</code> table reference
     */
    public TBLOrderline() {
        this("orderline", null);
    }

    /**
     * Create an aliased <code>netfoodz_dev.orderline</code> table reference
     */
    public TBLOrderline(String alias) {
        this(alias, ORDERLINE);
    }

    private TBLOrderline(String alias, Table<JQOrderlineRecord> aliased) {
        this(alias, aliased, null);
    }

    private TBLOrderline(String alias, Table<JQOrderlineRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return NetfoodzDev.NETFOODZ_DEV;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<JQOrderlineRecord> getPrimaryKey() {
        return Keys.KEY_ORDERLINE_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<JQOrderlineRecord>> getKeys() {
        return Arrays.<UniqueKey<JQOrderlineRecord>>asList(Keys.KEY_ORDERLINE_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<JQOrderlineRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<JQOrderlineRecord, ?>>asList(Keys.FK_ORDERITEM_ORDER, Keys.FK_ORDERITEM_PRODUCT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TBLOrderline as(String alias) {
        return new TBLOrderline(alias, this);
    }

    /**
     * Rename this table
     */
    public TBLOrderline rename(String name) {
        return new TBLOrderline(name, null);
    }
}
