package com.netfoodz.store.customer.domain.model;

import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.math.BigDecimal;

@EqualsAndHashCode
@Data
public class CartItem implements Entity {

	private Identity id;

	@NonNull
	private Identity productId;
	@NonNull
	private Identity cartId;
	@NonNull
	private BigDecimal price;
	private Integer quantity = 1;
	private String observation;

	public BigDecimal getTotalPrice() {
		return price.multiply(BigDecimal.valueOf(quantity));
	}
}
