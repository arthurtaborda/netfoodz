package com.netfoodz.store.customer.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.Customer;

public interface CustomerRepository {

	void save(Identity userId, Customer customer);

	Customer findOne(Identity customerId);
}
