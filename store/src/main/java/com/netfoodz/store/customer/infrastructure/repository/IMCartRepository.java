package com.netfoodz.store.customer.infrastructure.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.customer.domain.repository.CartRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("im")
@Repository
public class IMCartRepository implements CartRepository {

	private final Map<Identity, Cart> map = new HashMap<>();

	@Override
	public void save(Cart cart) {
		map.put(cart.getId(), cart);
	}

	@Override
	public Cart findCartFromCustomer(Identity userId) {
		return null;
	}

	@Override
	public Cart findOne(Identity cartId) {
		return null;
	}

	@Override
	public List<Identity> findIdsFromCustomer(Identity customerId) {
		return null;
	}
}
