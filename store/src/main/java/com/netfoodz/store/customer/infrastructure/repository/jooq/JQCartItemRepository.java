package com.netfoodz.store.customer.infrastructure.repository.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQCartitemDao;
import com.netfoodz.jooq.tables.pojos.JQCartitem;
import com.netfoodz.store.customer.domain.model.CartItem;
import com.netfoodz.store.customer.domain.repository.CartItemRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static com.netfoodz.jooq.tables.TBLCartitem.CARTITEM;

@Profile("jooq")
@Repository
public class JQCartItemRepository implements CartItemRepository {

	private DSLContext dsl;
	private JQCartitemDao dao;

	@Autowired
	public JQCartItemRepository(DSLContext dsl, JQCartitemDao dao) {
		this.dsl = dsl;
		this.dao = dao;
	}

	@Override
	public void save(CartItem item) {
		JQCartitem record = new JQCartitem();
		record.setId(item.getId());
		record.setAddedAt(Timestamp.from(Instant.now()));
		record.setCartId(item.getCartId().toString());
		record.setProductId(item.getProductId().toString());
		record.setPrice(item.getPrice().doubleValue());
		record.setObservation(item.getObservation());
		record.setQuantity(item.getQuantity());

		dao.insert(record);
	}

	@Override
	public void delete(Identity cartId, Identity productId) {
		dsl.delete(CARTITEM)
				.where(CARTITEM.CART_ID.eq(cartId.toString()))
				.and(CARTITEM.PRODUCT_ID.eq(productId.toString()))
				.execute();
	}

	@Override
	public void deleteAll(Identity cartId) {
		dsl.delete(CARTITEM)
				.where(CARTITEM.CART_ID.eq(cartId.toString()))
				.execute();
	}

	@Override
	public List<CartItem> findByCart(Identity cartId) {
		List<JQCartitem> records = dao.fetchByCartId(cartId.toString());
		List<CartItem> result = records.stream().map(r -> {
			CartItem item = new CartItem(Identity.of(r.getProductId()), cartId, BigDecimal.valueOf(r.getPrice()));
			item.setQuantity(r.getQuantity());
			item.setObservation(r.getObservation());
			return item;
		}).collect(Collectors.toList());
		return result;
	}

	@Override
	public void setQuantity(Identity cartId, Identity productId, Integer quantity) {
		dsl.update(CARTITEM)
				.set(CARTITEM.QUANTITY, quantity)
				.where(CARTITEM.CART_ID.eq(cartId.toString()))
				.and(CARTITEM.PRODUCT_ID.eq(productId.toString()))
				.execute();
	}
}
