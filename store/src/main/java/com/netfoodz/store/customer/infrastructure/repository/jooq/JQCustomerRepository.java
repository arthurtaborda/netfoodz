package com.netfoodz.store.customer.infrastructure.repository.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQCustomerDao;
import com.netfoodz.jooq.tables.pojos.JQCustomer;
import com.netfoodz.store.customer.domain.model.Customer;
import com.netfoodz.store.customer.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jooq")
@Repository
public class JQCustomerRepository implements CustomerRepository {

	private JQCustomerDao dao;

	@Autowired
	public JQCustomerRepository(JQCustomerDao dao) {
		this.dao = dao;
	}

	@Override
	public void save(Identity userId, Customer customer) {
		JQCustomer record = new JQCustomer();
		record.setId(customer.getId());
		record.setName(customer.getName());
		record.setUserId(userId.toString());

		dao.insert(record);
	}

	@Override
	public Customer findOne(Identity customerId) {
		JQCustomer record = dao.fetchOneById(customerId);
		if (record != null) {
			Customer customer = new Customer();
			customer.setName(record.getName());
			customer.setId(record.getId());
			return customer;
		}

		return null;
	}
}
