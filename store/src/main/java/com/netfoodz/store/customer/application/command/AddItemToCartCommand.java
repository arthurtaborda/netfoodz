package com.netfoodz.store.customer.application.command;

import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode
@Data
public class AddItemToCartCommand {

	@NonNull
	private Identity cartId;
	@NonNull
	private Identity productId;
	@NonNull
	private Integer quantity = 1;
	private String observation;
}
