package com.netfoodz.store.customer.application.command;

import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode
@Data
public class RemoveItemFromCartCommand {

	@NonNull
	private Identity cartId;
	@NonNull
	private Identity productId;
}
