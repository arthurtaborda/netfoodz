package com.netfoodz.store.customer.domain.model;

import com.netfoodz.common.domain.model.DomainEventPublisher;
import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.event.CartCleared;
import com.netfoodz.store.customer.domain.event.CartItemAdded;
import com.netfoodz.store.customer.domain.event.CartItemRemoved;
import com.netfoodz.store.customer.domain.exception.CartAlreadyEmptyException;
import com.netfoodz.store.customer.domain.exception.ItemAlreadyInCartException;
import com.netfoodz.store.customer.domain.exception.ProductNotInCartException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode
@Data
public class Cart implements Entity {

	private Identity id;

	@NonNull
	private Identity customerId;
	private final Map<Identity, CartItem> items = new HashMap<>();

	public CartItem add(Identity productId, BigDecimal price, Integer amount, String observation)
			throws ItemAlreadyInCartException {

		if (items.containsKey(productId)) {
			throw new ItemAlreadyInCartException();
		}

		CartItem item = new CartItem(productId, this.getId(), price);
		item.setQuantity(amount);
		item.setObservation(observation);
		items.put(productId, item);
		DomainEventPublisher.instance().publish(new CartItemAdded(customerId, item));

		return item;

	}

	public boolean isEmpty() {
		return items.isEmpty();
	}

	public CartItem get(Identity productId) {
		return items.get(productId);
	}

	public CartItem remove(Identity productId) throws ProductNotInCartException {
		if (!items.containsKey(productId)) {
			throw new ProductNotInCartException();
		}

		CartItem itemRemoved = items.remove(productId);

		DomainEventPublisher.instance().publish(new CartItemRemoved(customerId, itemRemoved));

		return itemRemoved;
	}

	public Collection<CartItem> clear() throws CartAlreadyEmptyException {
		if(items.isEmpty())
			throw new CartAlreadyEmptyException();

		Collection<CartItem> itemList = items.values();
		items.clear();

		DomainEventPublisher.instance().publish(new CartCleared(customerId));
		return itemList;
	}

	public Integer size() {
		return items.size();
	}

	public boolean contains(Identity productId) {
		return items.containsKey(productId);
	}

	public BigDecimal getTotalPrice() {
		BigDecimal price = BigDecimal.ZERO;
		for (CartItem item : items.values()) {
			price = price.add(item.getTotalPrice());
		}
		return price;
	}
}
