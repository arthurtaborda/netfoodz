package com.netfoodz.store.customer.domain.event;

import com.netfoodz.common.domain.model.AbstractDomainEvent;
import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.CartItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class CartItemAdded extends AbstractDomainEvent {

	private Identity customerId;
	private CartItem item;
}
