package com.netfoodz.store.customer.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.Cart;

import java.util.List;

public interface CartRepository {

	void save(Cart cart);

	Cart findCartFromCustomer(Identity userId);

	Cart findOne(Identity cartId);

	List<Identity> findIdsFromCustomer(Identity customerId);
}
