package com.netfoodz.store.customer.domain.event;

import com.netfoodz.common.domain.model.AbstractDomainEvent;
import com.netfoodz.common.domain.model.Identity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class CartCleared extends AbstractDomainEvent {

	private Identity customerId;
}
