package com.netfoodz.store.customer.application;

import com.netfoodz.store.customer.domain.model.Cart;

import java.util.List;
import java.util.stream.Collectors;

public class CartDTO {

	public static CartDTO of(Cart cart) {
		CartDTO dto = new CartDTO();
		dto.id = cart.getId().toString();
		dto.customerId = cart.getCustomerId().toString();
		dto.totalPrice = cart.getTotalPrice().doubleValue();
		dto.items = cart.getItems().values().stream().map(CartItemDTO::new).collect(Collectors.toList());

		return dto;
	}

	public String id;
	public Double totalPrice;
	public String customerId;
	public List<CartItemDTO> items;
}
