package com.netfoodz.store.customer.domain.service;

import com.netfoodz.store.customer.domain.exception.ItemAlreadyInCartException;
import com.netfoodz.store.customer.domain.exception.ProductNotInCatalogException;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.customer.domain.model.CartItem;
import com.netfoodz.store.product.domain.model.Product;
import org.springframework.stereotype.Component;

@Component
public class CartProductService {

	public CartItem addNewItem(Cart cart, Product product, Integer amount, String observation)
			throws ItemAlreadyInCartException {

		if (!product.isInCatalog()) {
			throw new ProductNotInCatalogException();
		}

		return cart.add(product.getId(), product.getPrice(), amount, observation);
	}
}
