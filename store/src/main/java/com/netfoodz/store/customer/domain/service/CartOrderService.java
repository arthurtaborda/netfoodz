package com.netfoodz.store.customer.domain.service;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.checkout.domain.model.Order;
import com.netfoodz.store.checkout.domain.model.OrderLine;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CartOrderService {

	public Order generateOrder(Cart cart) {
		Order order = new Order(cart.getCustomerId());
		cart.getItems()
				.values()
				.stream()
				.forEach(i -> {
					order.add(new OrderLine(i.getQuantity(), i.getProductId(), i.getPrice()));
					order.setCreatedAt(new Date());
				});

		order.setId(Identity.generate());

		return order;
	}
}
