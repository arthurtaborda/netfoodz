package com.netfoodz.store.customer.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.CartItem;

import java.util.List;

public interface CartItemRepository {

	void save(CartItem item);

	void delete(Identity cartId, Identity productId);

	void deleteAll(Identity cartId);

	List<CartItem> findByCart(Identity cartId);

	void setQuantity(Identity cartId, Identity productId, Integer quantity);
}
