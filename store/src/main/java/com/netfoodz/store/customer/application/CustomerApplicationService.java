package com.netfoodz.store.customer.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.customer.domain.model.Customer;
import com.netfoodz.store.customer.domain.repository.CartRepository;
import com.netfoodz.store.customer.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerApplicationService {

	private CartRepository cartRepository;
	private CustomerRepository customerRepository;

	@Autowired
	public CustomerApplicationService(CartRepository cartRepository, CustomerRepository customerRepository) {
		this.cartRepository = cartRepository;
		this.customerRepository = customerRepository;
	}

	public Customer createCustomer(Identity userId, String name) {
		Customer customer = new Customer();
		Identity customerId = Identity.generate();
		customer.setId(customerId);
		customer.setName(name);

		customerRepository.save(userId, customer);

		Cart cart = new Cart(customerId);
		cart.setId(Identity.generate());

		cartRepository.save(cart);

		return customer;
	}

	public CustomerDTO getCustomer(Identity customerId) {
		Customer customer = customerRepository.findOne(customerId);

		return CustomerDTO.of(customer);
	}
}
