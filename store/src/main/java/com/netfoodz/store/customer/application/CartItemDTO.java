package com.netfoodz.store.customer.application;

import com.netfoodz.store.customer.domain.model.CartItem;

public class CartItemDTO {

	public CartItemDTO(CartItem item) {
		this.productId = item.getProductId().toString();
		this.quantity = item.getQuantity();
		this.price = item.getPrice().doubleValue();
		this.totalPrice = item.getTotalPrice().doubleValue();
	}

	public String productId;
	public Integer quantity;
	public Double price;
	public Double totalPrice;
}
