package com.netfoodz.store.customer.application;

import com.netfoodz.store.customer.domain.model.Customer;

public class CustomerDTO {

	public static CustomerDTO of(Customer customer) {
		if(customer == null)
			return null;

		CustomerDTO dto = new CustomerDTO();
		dto.id = customer.getId().toString();
		dto.name = customer.getName();
		return dto;
	}

	public String id;
	public String name;
}
