package com.netfoodz.store.customer.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.authentication.application.SessionService;
import com.netfoodz.store.authentication.application.annotation.Authenticated;
import com.netfoodz.store.customer.application.command.AddItemToCartCommand;
import com.netfoodz.store.customer.application.command.RemoveItemFromCartCommand;
import com.netfoodz.store.customer.domain.exception.CartAlreadyEmptyException;
import com.netfoodz.store.customer.domain.exception.ItemAlreadyInCartException;
import com.netfoodz.store.customer.domain.exception.ProductNotInCartException;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.customer.domain.model.CartItem;
import com.netfoodz.store.customer.domain.repository.CartItemRepository;
import com.netfoodz.store.customer.domain.repository.CartRepository;
import com.netfoodz.store.customer.domain.service.CartProductService;
import com.netfoodz.store.product.domain.model.Product;
import com.netfoodz.store.product.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CartApplicationService {

	private SessionService session;
	private CartItemRepository cartItemRepository;
	private CartProductService cartProductService;
	private CartRepository cartRepository;
	private ProductRepository productRepository;

	@Autowired
	public CartApplicationService(SessionService sessionService, CartItemRepository cartItemRepository,
								  CartProductService cartProductService, CartRepository cartRepository,
								  ProductRepository productRepository) {
		this.session = sessionService;
		this.cartItemRepository = cartItemRepository;
		this.cartProductService = cartProductService;
		this.cartRepository = cartRepository;
		this.productRepository = productRepository;
	}

	public List<Identity> getCartsIds(Identity customerId) {
		List<Identity> ids = cartRepository.findIdsFromCustomer(customerId);

		return ids;
	}

	public CartDTO getCart(Identity cartId) {
		Cart cart = cartRepository.findOne(cartId);

		return CartDTO.of(cart);
	}

	@Authenticated
	public void addItemToCart(AddItemToCartCommand command) {
		Product product = productRepository.findById(command.getProductId());

		if(product == null) {
			throw new IllegalArgumentException("Product of socialAccountId " + command.getProductId() + " does not exist");
		}

		Cart cart = cartRepository.findOne(command.getCartId());

		try {
			CartItem item = cartProductService.addNewItem(cart, product, command.getQuantity(), command.getObservation());
			item.setId(Identity.generate());

			cartItemRepository.save(item);
		} catch (ItemAlreadyInCartException e) {
			Integer quantity = cart.get(command.getProductId()).getQuantity() + command.getQuantity();
			cartItemRepository.setQuantity(cart.getId(), product.getId(), quantity);
		}
	}

	@Authenticated
	public void removeItemFromCart(RemoveItemFromCartCommand command) throws ProductNotInCartException {
		cartItemRepository.delete(command.getCartId(), command.getProductId());
	}

	public void clearCart(Identity cartId) throws CartAlreadyEmptyException {
		cartItemRepository.deleteAll(cartId);
	}
}
