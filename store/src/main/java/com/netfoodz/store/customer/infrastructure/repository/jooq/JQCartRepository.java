package com.netfoodz.store.customer.infrastructure.repository.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.TBLCart;
import com.netfoodz.jooq.tables.daos.JQCartDao;
import com.netfoodz.jooq.tables.pojos.JQCart;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.customer.domain.model.CartItem;
import com.netfoodz.store.customer.domain.repository.CartRepository;
import com.netfoodz.store.customer.domain.repository.CartItemRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("jooq")
@Repository
public class JQCartRepository implements CartRepository {

	private DSLContext dsl;
	private JQCartDao dao;
	private CartItemRepository cartItemRepository;

	@Autowired
	public JQCartRepository(DSLContext dsl, JQCartDao dao, CartItemRepository cartItemRepository) {
		this.dsl = dsl;
		this.dao = dao;
		this.cartItemRepository = cartItemRepository;
	}


	@Override
	public void save(Cart cart) {
		JQCart record = new JQCart();
		record.setId(cart.getId());
		record.setCustomerId(cart.getCustomerId().toString());

		dao.insert(record);
	}

	@Override
	public Cart findCartFromCustomer(Identity customerId) {
		return null;
	}

	@Override
	public Cart findOne(Identity cartId) {
		JQCart record = dao.fetchOneById(cartId);
		if (record != null) {
			Cart cart = new Cart(Identity.of(record.getCustomerId()));
			cart.setId(record.getId());
			Map<Identity, CartItem> items = getItems(cartId);
			if (items != null) {
				cart.getItems().putAll(items);
			}
			return cart;
		}

		return null;
	}

	@Override
	public List<Identity> findIdsFromCustomer(Identity customerId) {
		TBLCart cart = TBLCart.CART;
		return dsl.selectFrom(cart)
				.where(cart.CUSTOMER_ID.eq(customerId.toString()))
				.fetch(cart.ID);
	}

	private Map<Identity, CartItem> getItems(Identity cartId) {
		List<CartItem> list = cartItemRepository.findByCart(cartId);
		if (list != null) {
			return list
					.stream()
					.collect(HashMap<Identity, CartItem>::new,
							(map, item) -> map.put(item.getProductId(), item),
							HashMap<Identity, CartItem>::putAll);
		}

		return null;
	}
}
