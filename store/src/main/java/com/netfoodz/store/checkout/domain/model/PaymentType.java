package com.netfoodz.store.checkout.domain.model;

public enum PaymentType {
	GIFT_CARD, CREDIT_CARD, CUSTOMER_CREDIT
}
