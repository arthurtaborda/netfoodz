package com.netfoodz.store.checkout.domain.model;

import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode
@RequiredArgsConstructor
@Getter
@Setter
public class Order implements Entity {

	private Identity id;

	@NonNull
	private Identity customerId;
	private Date createdAt;
	private OrderState orderState = OrderState.WAITING_FOR_PAYMENT;
	private final List<OrderLine> orderLines = new ArrayList<>();


	public void add(OrderLine orderLine) {
		orderLines.add(orderLine);
	}

	public BigDecimal getTotalPrice() {
		BigDecimal total = BigDecimal.ZERO;
		for (OrderLine orderLine : orderLines) {
			total = total.add(orderLine.getPrice().multiply(BigDecimal.valueOf(orderLine.getQuantity())));
		}
		return total;
	}

}
