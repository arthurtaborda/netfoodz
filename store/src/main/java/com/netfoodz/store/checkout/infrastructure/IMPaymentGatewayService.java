package com.netfoodz.store.checkout.infrastructure;

import com.netfoodz.store.checkout.domain.exception.PaymentGatewayException;
import com.netfoodz.store.checkout.domain.model.CreditCard;
import com.netfoodz.store.checkout.domain.service.PaymentGatewayService;
import com.netfoodz.store.checkout.domain.service.PaymentResponseDTO;
import org.springframework.stereotype.Service;

@Service
public class IMPaymentGatewayService implements PaymentGatewayService {

	@Override
	public PaymentResponseDTO payWithCreditCard(CreditCard creditCard) throws PaymentGatewayException {
		return null;
	}
}
