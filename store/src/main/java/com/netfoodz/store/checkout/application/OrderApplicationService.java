package com.netfoodz.store.checkout.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.domain.model.Order;
import com.netfoodz.store.checkout.domain.model.OrderState;
import com.netfoodz.store.checkout.domain.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderApplicationService {

	private OrderRepository orderRepository;

	@Autowired
	public OrderApplicationService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}


	public List<OrderDTO> getOrders(Identity customerId) {
		List<Order> orders = orderRepository.findByCustomer(customerId);
		if(orders != null) {
			return orders.stream().map(OrderDTO::of).collect(Collectors.toList());
		}

		return null;
	}

	public OrderDTO get(Identity orderId) {
		Order order = orderRepository.findOne(orderId);
		if (order != null) {
			return OrderDTO.of(order);
		}

		return null;
	}

	public void setInProcess(Identity orderId) {
		orderRepository.setStatus(orderId, OrderState.IN_PROCESS);
	}
}
