package com.netfoodz.store.checkout.application;

import com.netfoodz.common.domain.util.Constants;
import com.netfoodz.store.checkout.domain.model.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderDTO {

	public static OrderDTO of(Order order) {
		OrderDTO dto = new OrderDTO();
		dto.id = order.getId().toString();
		dto.customerId = order.getCustomerId().toString();
		dto.totalPrice = order.getTotalPrice().doubleValue();
		dto.lines = order.getOrderLines().stream().map(OrderLineDTO::of).collect(Collectors.toList());
		dto.state = order.getOrderState().toString();
		dto.createdAt = Constants.DATE_FORMAT.format(order.getCreatedAt());

		return dto;
	}

	public String id;
	public String state;
	public String createdAt;
	public String customerId;
	public Double totalPrice;
	public List<OrderLineDTO> lines;
}
