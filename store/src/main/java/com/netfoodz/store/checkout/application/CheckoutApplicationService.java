package com.netfoodz.store.checkout.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.domain.exception.CheckoutException;
import com.netfoodz.store.checkout.domain.model.Order;
import com.netfoodz.store.checkout.domain.model.OrderLine;
import com.netfoodz.store.checkout.domain.repository.OrderLineRepository;
import com.netfoodz.store.checkout.domain.repository.OrderRepository;
import com.netfoodz.store.customer.application.CartApplicationService;
import com.netfoodz.store.customer.domain.model.Cart;
import com.netfoodz.store.customer.domain.repository.CartRepository;
import com.netfoodz.store.customer.domain.service.CartOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckoutApplicationService {

	private CartOrderService cartOrderService;
	private CartApplicationService cartService;
	private CartRepository cartRepository;
	private OrderRepository orderRepository;
	private OrderLineRepository orderLineRepository;

	@Autowired
	public CheckoutApplicationService(CartOrderService cartOrderService, CartApplicationService cartService, CartRepository cartRepository, OrderRepository orderRepository, OrderLineRepository orderLineRepository) {
		this.cartOrderService = cartOrderService;
		this.cartService = cartService;
		this.cartRepository = cartRepository;
		this.orderRepository = orderRepository;
		this.orderLineRepository = orderLineRepository;
	}

	public OrderDTO checkout(Identity cartId) {
		Cart cart = cartRepository.findOne(cartId);

		if(cart.isEmpty()) {
			throw new CheckoutException("Can't checkout an empty cart");
		}

		Order order = cartOrderService.generateOrder(cart);
		Identity orderId = Identity.generate();
		order.setId(orderId);

		orderRepository.save(order);
		for (OrderLine orderLine : order.getOrderLines()) {
			orderLine.setId(Identity.generate());
			orderLineRepository.save(orderId, orderLine);
		}

		cartService.clearCart(cartId);

		return  OrderDTO.of(order);
	}
}
