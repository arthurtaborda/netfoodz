package com.netfoodz.store.checkout.domain.service;

import com.netfoodz.store.checkout.domain.exception.PaymentGatewayException;
import com.netfoodz.store.checkout.domain.model.CreditCard;

public interface PaymentGatewayService {

	public PaymentResponseDTO payWithCreditCard(CreditCard creditCard) throws PaymentGatewayException;
}
