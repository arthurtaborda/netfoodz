package com.netfoodz.store.checkout.application;

import com.netfoodz.store.checkout.domain.model.OrderLine;

public class OrderLineDTO {

	public static OrderLineDTO of(OrderLine item) {
		OrderLineDTO dto = new OrderLineDTO();
		dto.productId = item.getProductId().toString();
		dto.quantity = item.getQuantity();
		dto.price = item.getPrice().doubleValue();
		dto.totalPrice = item.getTotalPrice().doubleValue();

		return dto;
	}

	public String productId;
	public Integer quantity;
	public Double price;
	public Double totalPrice;
}
