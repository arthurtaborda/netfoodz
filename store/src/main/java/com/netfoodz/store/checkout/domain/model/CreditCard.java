package com.netfoodz.store.checkout.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

@Getter
@ToString(callSuper = true)
@AllArgsConstructor
public class CreditCard {

	private CreditCardNumber number;
	private String cardHolderName;

	private Month expiryMonth;
	private Year expiryYear;
	private Integer cvv;

	public boolean isValid() {
		return isValid(LocalDate.now());
	}

	public boolean isValid(LocalDate date) {
		return date != null && getExpirationDate().isAfter(date);
	}

	public LocalDate getExpirationDate() {
		return LocalDate.of(expiryYear.getValue(), expiryMonth, 1);
	}

	protected void setExpirationDate(LocalDate date) {
		this.expiryYear = Year.of(date.getYear());
		this.expiryMonth = date.getMonth();
	}
}
