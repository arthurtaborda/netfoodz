package com.netfoodz.store.checkout.domain.repository;

import com.netfoodz.store.checkout.domain.model.Payment;

public interface PaymentRepository {

	public void save(Payment payment);
}
