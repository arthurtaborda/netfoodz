package com.netfoodz.store.checkout.domain.exception;

public class OrderStateException extends RuntimeException {
	public OrderStateException() {
	}

	public OrderStateException(String s) {
		super(s);
	}

	public OrderStateException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public OrderStateException(Throwable throwable) {
		super(throwable);
	}

	public OrderStateException(String s, Throwable throwable, boolean b, boolean b1) {
		super(s, throwable, b, b1);
	}
}
