package com.netfoodz.store.checkout.domain.model;

import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.*;

import java.math.BigDecimal;
import java.util.Currency;

@EqualsAndHashCode
@RequiredArgsConstructor
@Getter
@Setter
public class Payment implements Entity {

	private Identity id;

	@NonNull
	private Identity orderId;
	@NonNull
	private PaymentType paymentType;
	@NonNull
	private Currency currency;
	@NonNull
	private BigDecimal amount;

}
