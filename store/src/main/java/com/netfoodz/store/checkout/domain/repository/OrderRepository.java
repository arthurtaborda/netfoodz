package com.netfoodz.store.checkout.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.domain.model.Order;
import com.netfoodz.store.checkout.domain.model.OrderState;

import java.util.List;

public interface OrderRepository {

	void save(Order order);

	Order findOne(Identity orderId);

	void setStatus(Identity orderId, OrderState status);

	List<Order> findByCustomer(Identity customerId);
}
