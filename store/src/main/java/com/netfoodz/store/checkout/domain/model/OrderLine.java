package com.netfoodz.store.checkout.domain.model;

import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.*;

import java.math.BigDecimal;

@Data
public class OrderLine implements Entity {

	private Identity id;
	@NonNull
	private Integer quantity;
	@NonNull
	private Identity productId;
	@NonNull
	private BigDecimal price;
	private String observation;

	public BigDecimal getTotalPrice() {
		return price.multiply(BigDecimal.valueOf(quantity));
	}
}
