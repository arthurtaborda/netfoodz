package com.netfoodz.store.checkout.domain.model;

public enum OrderState {
	WAITING_FOR_PAYMENT, PAID, IN_PROCESS, CANCELLED, COMPLETE
}
