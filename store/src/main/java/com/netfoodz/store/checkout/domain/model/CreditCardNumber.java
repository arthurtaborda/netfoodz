package com.netfoodz.store.checkout.domain.model;

import lombok.Data;

@Data
public class CreditCardNumber {

	private static final String regex = "[0-9]{16}";
	private final String number;

	public CreditCardNumber(String number) {

		if (!isValid(number)) {
			throw new IllegalArgumentException(String.format("Invalid credit card NUMBER %s!", number));
		}

		this.number = number;
	}

	public static boolean isValid(String number) {
		return number != null && number.matches(regex);
	}
}
