package com.netfoodz.store.checkout.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.domain.exception.OrderStateException;
import com.netfoodz.store.checkout.domain.exception.PaymentGatewayException;
import com.netfoodz.store.checkout.domain.model.CreditCard;
import com.netfoodz.store.checkout.domain.model.Order;
import com.netfoodz.store.checkout.domain.model.OrderState;
import com.netfoodz.store.checkout.domain.repository.OrderRepository;
import com.netfoodz.store.checkout.domain.service.PaymentGatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentApplicationService {

	private OrderRepository orderRepository;
	private PaymentGatewayService paymentGateway;

	@Autowired
	public PaymentApplicationService(OrderRepository orderRepository, PaymentGatewayService paymentGateway) {
		this.orderRepository = orderRepository;
		this.paymentGateway = paymentGateway;
	}


	public void pay(Identity orderId, CreditCard creditCard) throws PaymentGatewayException {
		Order order = orderRepository.findOne(orderId);

		if (!order.getOrderState().equals(OrderState.WAITING_FOR_PAYMENT)) {
			throw new OrderStateException("Order " + orderId.toString() + " should have state WAITING_FOR_PAYMENT. " +
					"Current state is " + order.getOrderState());
		}

		paymentGateway.payWithCreditCard(creditCard);

		orderRepository.setStatus(orderId, OrderState.PAID);
	}
}
