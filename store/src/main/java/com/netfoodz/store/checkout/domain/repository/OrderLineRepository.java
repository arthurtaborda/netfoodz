package com.netfoodz.store.checkout.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.checkout.domain.model.OrderLine;

import java.util.List;

public interface OrderLineRepository {

	void save(Identity orderId, OrderLine item);

	List<OrderLine> findByOrder(Identity orderId);
}
