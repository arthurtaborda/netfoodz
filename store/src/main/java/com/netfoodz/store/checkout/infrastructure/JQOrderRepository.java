package com.netfoodz.store.checkout.infrastructure;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQOrderDao;
import com.netfoodz.jooq.tables.pojos.JQOrder;
import com.netfoodz.store.checkout.domain.model.Order;
import com.netfoodz.store.checkout.domain.model.OrderLine;
import com.netfoodz.store.checkout.domain.model.OrderState;
import com.netfoodz.store.checkout.domain.repository.OrderLineRepository;
import com.netfoodz.store.checkout.domain.repository.OrderRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.netfoodz.jooq.tables.TBLOrder.ORDER;


@Repository
public class JQOrderRepository implements OrderRepository {

	private DSLContext dsl;
	private JQOrderDao dao;
	private OrderLineRepository orderLineRepository;

	@Autowired
	public JQOrderRepository(DSLContext dsl, JQOrderDao dao, OrderLineRepository orderLineRepository) {
		this.dsl = dsl;
		this.dao = dao;
		this.orderLineRepository = orderLineRepository;
	}

	@Override
	public void save(Order order) {
		JQOrder record = new JQOrder();
		record.setId(order.getId());
		record.setCustomerId(order.getCustomerId().toString());
		record.setCreatedAt(new Timestamp(order.getCreatedAt().getTime()));
		record.setState(OrderState.WAITING_FOR_PAYMENT.toString());

		dao.insert(record);
	}

	@Override
	public Order findOne(Identity orderId) {
		JQOrder record = dao.fetchOneById(orderId);
		if (record != null) {
			return getOrder(record);
		}

		return null;
	}

	private Order getOrder(JQOrder record) {
		Order order = new Order(Identity.of(record.getCustomerId()));
		order.setId(record.getId());
		order.setOrderState(OrderState.valueOf(record.getState()));
		order.setCreatedAt(new Date(record.getCreatedAt().getTime()));
		List<OrderLine> lines = orderLineRepository.findByOrder(record.getId());
		if (lines != null) {
			order.getOrderLines().addAll(lines);
		}
		return order;
	}

	@Override
	public void setStatus(Identity orderId, OrderState status) {
		dsl.update(ORDER).set(ORDER.STATE, status.toString()).execute();
	}

	@Override
	public List<Order> findByCustomer(Identity customerId) {
		List<JQOrder> records = dao.fetchByCustomerId(customerId.toString());

		if (records != null) {
			return records.stream().map(this::getOrder).collect(Collectors.toList());
		}

		return null;
	}
}
