package com.netfoodz.store.checkout.domain.exception;

public class PaymentGatewayException extends Exception {
	public PaymentGatewayException() {
	}

	public PaymentGatewayException(String s) {
		super(s);
	}

	public PaymentGatewayException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public PaymentGatewayException(Throwable throwable) {
		super(throwable);
	}

	public PaymentGatewayException(String s, Throwable throwable, boolean b, boolean b1) {
		super(s, throwable, b, b1);
	}
}
