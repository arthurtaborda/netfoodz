package com.netfoodz.store.checkout.domain.exception;

public class CheckoutException extends RuntimeException {
	public CheckoutException() {
	}

	public CheckoutException(String s) {
		super(s);
	}

	public CheckoutException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public CheckoutException(Throwable throwable) {
		super(throwable);
	}

	public CheckoutException(String s, Throwable throwable, boolean b, boolean b1) {
		super(s, throwable, b, b1);
	}
}
