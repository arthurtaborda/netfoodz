package com.netfoodz.store.checkout.infrastructure;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQOrderlineDao;
import com.netfoodz.jooq.tables.pojos.JQOrderline;
import com.netfoodz.store.checkout.domain.model.OrderLine;
import com.netfoodz.store.checkout.domain.repository.OrderLineRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;


@Repository
public class JQOrderLineRepository implements OrderLineRepository {

	private DSLContext dsl;
	private JQOrderlineDao dao;

	@Autowired
	public JQOrderLineRepository(DSLContext dsl, JQOrderlineDao dao) {
		this.dsl = dsl;
		this.dao = dao;
	}

	@Override
	public void save(Identity orderId, OrderLine item) {
		JQOrderline record = new JQOrderline();
		record.setId(item.getId());
		record.setQuantity(item.getQuantity());
		record.setPrice(item.getPrice().doubleValue());
		record.setObservation(item.getObservation());
		record.setOrderId(orderId.toString());
		record.setProductId(item.getProductId().toString());

		dao.insert(record);
	}

	@Override
	public List<OrderLine> findByOrder(Identity orderId) {
		List<JQOrderline> records = dao.fetchByOrderId(orderId.toString());
		List<OrderLine> result = records.stream().map(r -> {
			OrderLine item = new OrderLine(r.getQuantity(), Identity.of(r.getProductId()),
					BigDecimal.valueOf(r.getPrice()));
			item.setQuantity(r.getQuantity());
			item.setObservation(r.getObservation());
			return item;
		}).collect(Collectors.toList());
		return result;
	}
}
