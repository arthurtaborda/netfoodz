package com.netfoodz.store.mobile.infrastructure.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gcm.server.*;
import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import com.netfoodz.store.mobile.domain.model.NotificationRequest;
import com.netfoodz.store.mobile.domain.model.NotificationResult;
import com.netfoodz.store.mobile.domain.model.NotificationStatus;
import com.netfoodz.store.mobile.domain.service.NotificationClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class GCMClient implements NotificationClient {

	private ObjectMapper mapper;
	private Sender sender;

	private Map<String, NotificationResult> successfulNotifications;
	private Map<String, NotificationResult> failedNotifications;

	@Autowired
	public GCMClient(Sender sender) {
		this.sender = sender;
		this.mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
	}

	@Override
	public Map<String, NotificationResult> getSuccessfulNotifications() {
		return successfulNotifications;
	}

	@Override
	public Map<String, NotificationResult> getFailedNotifications() {
		return failedNotifications;
	}

	@Override
	public void send(NotificationRequest notification, String[] devicesIds) throws IOException {
		this.failedNotifications = new HashMap<>();
		this.successfulNotifications = new HashMap<>();
		MulticastResult multicastResult;

		String notificationJson = mapper.valueToTree(notification).toString();
		Message message = new Message.Builder().addData("message", notificationJson).delayWhileIdle(true).timeToLive(86400).build();

		multicastResult = sender.send(message, Arrays.asList(devicesIds), 5);

		List<Result> results = multicastResult.getResults();
		for (int i = 0; i < results.size(); i++) {
			NotificationResult notificationResult = new NotificationResult();
			Result r = results.get(i);
			if(r.getMessageId() == null) {
				notificationResult.setStatus(NotificationStatus.SERVER_ERROR);
				notificationResult.setErrorMessage(r.getErrorCodeName());
				if (Constants.ERROR_NOT_REGISTERED.equals(notificationResult.getErrorMessage())) {
					notificationResult.setDeviceDeactivated(true);
				}
				failedNotifications.put(devicesIds[i], notificationResult);
			} else {
				notificationResult.setStatus(NotificationStatus.SUCCESS);
				notificationResult.setId(r.getMessageId());
				notificationResult.setDeviceDeactivated(false);
				successfulNotifications.put(devicesIds[i], notificationResult);
			}

		}
	}

	@Override
	public MobilePlatform getPlatform() {
		return MobilePlatform.ANDROID;
	}
}
