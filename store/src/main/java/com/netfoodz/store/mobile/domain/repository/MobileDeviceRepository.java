package com.netfoodz.store.mobile.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.mobile.domain.model.Location;
import com.netfoodz.store.mobile.domain.model.MobileDevice;
import com.netfoodz.store.mobile.domain.model.MobilePlatform;

public interface MobileDeviceRepository {

	void updateUserId(Identity deviceId, Identity userId);

	void updateLocation(Identity deviceId, Location location);

	boolean exists(Identity identity);

	void save(MobileDevice mobileDevice);

	MobileDevice findByDeviceIdAndPlatform(Identity deviceId, MobilePlatform platform);
}
