package com.netfoodz.store.mobile.infrastructure.repository.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.TBLMobiledevice;
import com.netfoodz.jooq.tables.daos.JQMobiledeviceDao;
import com.netfoodz.jooq.tables.pojos.JQMobiledevice;
import com.netfoodz.jooq.tables.records.JQMobiledeviceRecord;
import com.netfoodz.store.mobile.domain.model.Location;
import com.netfoodz.store.mobile.domain.model.MobileDevice;
import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import com.netfoodz.store.mobile.domain.repository.MobileDeviceRepository;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.UpdateSetMoreStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import static com.netfoodz.jooq.tables.TBLMobiledevice.MOBILEDEVICE;

@Profile("jooq")
@Repository
public class JQMobileDeviceRepository implements MobileDeviceRepository {

	private DSLContext dsl;
	private JQMobiledeviceDao dao;

	@Autowired
	public JQMobileDeviceRepository(DSLContext dsl, JQMobiledeviceDao dao) {
		this.dsl = dsl;
		this.dao = dao;
	}

	@Override
	public void updateUserId(Identity deviceId, Identity userId) {
		TBLMobiledevice md = MOBILEDEVICE;
		UpdateSetMoreStep<JQMobiledeviceRecord> update = dsl.update(md)
				.set(md.CURRENT_USER_ID, userId != null ? userId.toString() : null);

		if(userId != null)
			update.set(md.PREVIOUS_USER_ID, userId.toString());

		update.where(ideq(deviceId)).execute();
	}

	@Override
	public void updateLocation(Identity deviceId, Location location) {
		Assert.notNull(deviceId);
		if(location == null || location.getLat() == null || location.getLng() == null) {
			location = new Location();
		}

		TBLMobiledevice md = MOBILEDEVICE;
		dsl.update(md)
				.set(md.LAT, location.getLat())
				.set(md.LNG, location.getLng())
				.where(ideq(deviceId))
				.execute();
	}

	@Override
	public boolean exists(Identity identity) {
		TBLMobiledevice md = MOBILEDEVICE;
		return dsl.fetchExists(md, ideq(identity));
	}

	@Override
	public void save(MobileDevice mobileDevice) {

		JQMobiledevice record = new JQMobiledevice();
		record.setId(mobileDevice.getId());
		record.setActive(mobileDevice.isActive() ? Byte.valueOf("1") : Byte.valueOf("0"));

		if (mobileDevice.getCurrentUserId() != null) {
			record.setCurrentUserId(mobileDevice.getCurrentUserId().toString());
		}
		if (mobileDevice.getPreviousUserId() != null) {
			record.setPreviousUserId(mobileDevice.getPreviousUserId().toString());
		}

		record.setPlatform(mobileDevice.getPlatform().toString());
		if (mobileDevice.getLocation() != null) {
			record.setLat(mobileDevice.getLocation().getLat());
			record.setLng(mobileDevice.getLocation().getLng());
		}

		dao.insert(record);
	}

	@Override
	public MobileDevice findByDeviceIdAndPlatform(Identity deviceId, MobilePlatform platform) {
		JQMobiledevice record = dsl.select()
				.from(MOBILEDEVICE)
				.where(MOBILEDEVICE.ID.eq(deviceId))
				.and(MOBILEDEVICE.PLATFORM.eq(platform.toString()))
				.fetchOneInto(JQMobiledevice.class);

		MobileDevice mobileDevice = new MobileDevice();
		mobileDevice.setId(record.getId());
		mobileDevice.setActive(record.getActive()==1);
		mobileDevice.setCurrentUserId(Identity.of(record.getCurrentUserId()));
		if (record.getLat() != null && record.getLng() != null) {
			mobileDevice.setLocation(new Location(record.getLat(), record.getLng()));
		}
		mobileDevice.setPreviousUserId(Identity.of(record.getPreviousUserId()));


		return mobileDevice;
	}

	private Condition ideq(Identity deviceId) {
		TBLMobiledevice md = MOBILEDEVICE;
		return md.ID.eq(deviceId);
	}
}
