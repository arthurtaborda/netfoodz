package com.netfoodz.store.mobile.domain.model;

import lombok.Data;

@Data
public class NotificationResult {

	private String id;
	private NotificationStatus status;
	private String errorMessage;
	private boolean deviceDeactivated;
}
