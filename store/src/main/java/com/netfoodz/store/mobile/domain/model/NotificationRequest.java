package com.netfoodz.store.mobile.domain.model;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.mobile.domain.service.NotificationClient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class NotificationRequest {

	private String eventType;
	private String title;
	private String message;
	private String groupId;

	public List<NotificationSent> sendToDevices(NotificationClient client, Integer batchSize, String... devicesIds) {
		Assert.notEmpty(devicesIds);
		Assert.notNull(client);

		MobilePlatform platform = client.getPlatform();
		Map<String, NotificationResult> results;
		List<NotificationSent> notifications = new ArrayList<>();

		Batch<String> parts = new Batch<>(Arrays.asList(devicesIds), batchSize);
		for (List<String> batch : parts) {
			try {
				log.info(platform.toString() + ": Sending notification to " +
						batch.size() + " devices. Notification groupId: " + groupId);
				results = sendMessageToDevices(client, batch.stream().toArray(String[]::new));

				notifications.addAll(this.getSuccessNotifications(results, platform));
			} catch (IOException e) {
				log.error("Error sending notification to devices", e);
				notifications.addAll(this.getErrorNotifications(batch, e, platform));
			}
		}

		return notifications;
	}

	Map<String, NotificationResult> sendMessageToDevices(NotificationClient client, String[] devicesIds) throws IOException {

		client.send(this, devicesIds);

		Map<String, NotificationResult> notificationResultMap = new HashMap<>();
		Map<String, NotificationResult> failedNotifications = client.getFailedNotifications();
		Map<String, NotificationResult> successfulNotifications = client.getSuccessfulNotifications();

		for (String device : devicesIds) {
			NotificationResult notificationResult = null;
			if (failedNotifications != null) {
				notificationResult = failedNotifications.get(device);
			}

			if(notificationResult == null && successfulNotifications != null)
				notificationResult = successfulNotifications.get(device);

			if (notificationResult == null) {
				notificationResult = new NotificationResult();
				notificationResult.setStatus(NotificationStatus.SUCCESS);
			}

			notificationResultMap.put(device, notificationResult);
		}

		return notificationResultMap;
	}

	private List<NotificationSent> getErrorNotifications(Collection<String> devices,
														   Exception e,
														   MobilePlatform platform) {
		List<NotificationSent> sent = new ArrayList<>();
		if (devices == null || devices.isEmpty())
			return sent;

		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));

		for (String device : devices) {
			NotificationSent notification = getDefaultNotificationSent(platform, device);
			notification.setStatus(NotificationStatus.SEND_ERROR);
			notification.setErrorCodeName("Failed to send notification to server");
			notification.setDeviceDeactivated(false);
			notification.setStackTrace(sw.toString());
			sent.add(notification);
		}

		return sent;
	}

	private NotificationSent getDefaultNotificationSent(MobilePlatform platform, String device) {
		NotificationSent notification = new NotificationSent();
		notification.setDeviceId(Identity.of(device));
		notification.setPlatform(platform);
		notification.setNotificationGroupId(groupId);
		return notification;
	}


	private List<NotificationSent> getSuccessNotifications(Map<String, NotificationResult> results,
															  MobilePlatform platform) {
		List<NotificationSent> sent = new ArrayList<>();
		if (results == null || results.isEmpty())
			return sent;

		for (String device : results.keySet()) {
			NotificationResult r = results.get(device);
			NotificationSent notification = getDefaultNotificationSent(platform, device);
			notification.setNotificationId(r.getId());
			notification.setStatus(r.getStatus());
			notification.setErrorCodeName(r.getErrorMessage());
			notification.setDeviceDeactivated(r.isDeviceDeactivated());
			sent.add(notification);
		}

		return sent;
	}
}
