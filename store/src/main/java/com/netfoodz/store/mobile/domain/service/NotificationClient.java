package com.netfoodz.store.mobile.domain.service;


import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import com.netfoodz.store.mobile.domain.model.NotificationRequest;
import com.netfoodz.store.mobile.domain.model.NotificationResult;

import java.io.IOException;
import java.util.Map;

public interface NotificationClient {

	Map<String, NotificationResult> getSuccessfulNotifications();

	Map<String, NotificationResult> getFailedNotifications();

	void send(NotificationRequest notification, String[] devices) throws IOException;

	MobilePlatform getPlatform();
}
