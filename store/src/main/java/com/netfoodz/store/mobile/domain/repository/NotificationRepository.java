package com.netfoodz.store.mobile.domain.repository;

import com.netfoodz.store.mobile.domain.model.NotificationSent;

public interface NotificationRepository {

	void save(NotificationSent notification);
}
