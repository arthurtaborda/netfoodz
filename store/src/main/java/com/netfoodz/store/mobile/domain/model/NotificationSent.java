package com.netfoodz.store.mobile.domain.model;

import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class NotificationSent {

	private Identity deviceId;
	private String notificationId;
	private String notificationGroupId;
	private String errorMessage;
	private boolean deviceDeactivated;
	private String errorCodeName;
	private String stackTrace;
	private NotificationStatus status;
	private MobilePlatform platform;
}
