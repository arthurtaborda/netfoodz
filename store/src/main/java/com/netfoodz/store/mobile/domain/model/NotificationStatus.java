package com.netfoodz.store.mobile.domain.model;

public enum NotificationStatus {
	SEND_ERROR,
	SERVER_ERROR,
	SUCCESS
}
