package com.netfoodz.store.mobile.infrastructure.repository.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQNotificationDao;
import com.netfoodz.jooq.tables.pojos.JQNotification;
import com.netfoodz.store.mobile.domain.model.NotificationSent;
import com.netfoodz.store.mobile.domain.repository.NotificationRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("jooq")
@Repository
public class JQNotificationRepository implements NotificationRepository {

	private DSLContext dsl;
	private JQNotificationDao dao;

	@Autowired
	public JQNotificationRepository(DSLContext dsl, JQNotificationDao dao) {
		this.dsl = dsl;
		this.dao = dao;
	}

	@Override
	public void save(NotificationSent notification) {
		JQNotification record = new JQNotification();
		record.setId(Identity.generate());
		record.setNotificationId(notification.getNotificationId());
		record.setDeviceId(notification.getDeviceId().toString());
		record.setDeviceDeactivated(notification.isDeviceDeactivated() ? Byte.valueOf("1") : Byte.valueOf("0"));
		record.setErrorCodename(notification.getErrorCodeName());
		record.setPlatform(notification.getPlatform().toString());
		record.setErrorMessage(notification.getErrorMessage());
		record.setStackTrace(notification.getStackTrace());
		record.setStatus(notification.getStatus().toString());
		record.setNotificationGroupId(notification.getNotificationGroupId());

		dao.insert(record);
	}
}
