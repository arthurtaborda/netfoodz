package com.netfoodz.store.mobile.domain.model;

public enum MobilePlatform {
	APPLE,
	ANDROID
}
