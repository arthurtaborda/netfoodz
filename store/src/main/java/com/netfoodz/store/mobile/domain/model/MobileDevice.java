package com.netfoodz.store.mobile.domain.model;


import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.mobile.domain.service.NotificationClient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode
@Data
public class MobileDevice implements Entity {

	private Identity id;
	private Identity currentUserId;
	private Identity previousUserId;

	private Location location;
	private boolean active;

	private MobilePlatform platform;

	public boolean isUserAnonymous() {
		return currentUserId == null;
	}

	public NotificationSent sendNotification(NotificationClient client, NotificationRequest request) {
		List<NotificationSent> notificationsSent = request.sendToDevices(client, 1, id.toString());
		if(notificationsSent != null && notificationsSent.size() > 0) {
			return notificationsSent.get(0);
		}
		return null;
	}
}
