package com.netfoodz.store.mobile.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.authentication.application.SessionService;
import com.netfoodz.store.authentication.domain.exception.NoUserAuthenticatedException;
import com.netfoodz.store.mobile.domain.model.*;
import com.netfoodz.store.mobile.domain.repository.MobileDeviceRepository;
import com.netfoodz.store.mobile.domain.repository.NotificationRepository;
import com.netfoodz.store.mobile.domain.service.NotificationClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Slf4j
@Service
public class MobileApplicationService {

	private List<NotificationClient> notificationClients;
	private SessionService sessionService;
	private MobileDeviceRepository mobileDeviceRepository;
	private NotificationRepository notificationRepository;

	@Autowired
	public MobileApplicationService(List<NotificationClient> notificationClients, SessionService sessionService, MobileDeviceRepository mobileDeviceRepository, NotificationRepository notificationRepository) {
		this.notificationClients = notificationClients;
		this.sessionService = sessionService;
		this.mobileDeviceRepository = mobileDeviceRepository;
		this.notificationRepository = notificationRepository;
	}

	public void updateLocation(Identity deviceId, Location location, MobilePlatform platform) {
		Assert.notNull(deviceId);

		Identity loggedUserId = null;
		try {
			loggedUserId = sessionService.getUserId();
		} catch (NoUserAuthenticatedException ignored) {
		}

		if(mobileDeviceRepository.exists(deviceId)) {
			mobileDeviceRepository.updateLocation(deviceId, location);
			mobileDeviceRepository.updateUserId(deviceId, loggedUserId);
		} else {
			MobileDevice mobileDevice = new MobileDevice();
			mobileDevice.setActive(true);
			mobileDevice.setLocation(location);
			mobileDevice.setPlatform(platform);
			mobileDevice.setId(deviceId);
			mobileDevice.setCurrentUserId(loggedUserId);
			mobileDeviceRepository.save(mobileDevice);
		}
	}

	public void sendOrderInProgressNotification(Identity deviceId, MobilePlatform platform, String orderCode) {
		MobileDevice device = mobileDeviceRepository.findByDeviceIdAndPlatform(deviceId, platform);

		NotificationRequest notification = new NotificationRequest("ORDER_IN_PROGRESS", "Your order is ready", "Your order code is " + orderCode + ". Present in the balcony to get it", Identity.generate().toString());

		for (NotificationClient notificationClient : notificationClients) {
			NotificationSent notificationSent = device.sendNotification(notificationClient, notification);
			log.info(notificationSent.toString());
			notificationRepository.save(notificationSent);
		}
	}
}
