package com.netfoodz.store.authentication.domain.model;

import com.netfoodz.common.domain.model.EmailAddress;
import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@EqualsAndHashCode
@Data
public class FacebookUser implements Entity, SocialUser {

	private Identity id;

	private Identity customerId;
	private String socialAccountId;
	private String name;
	private EmailAddress email;
	private Cover cover;
	private boolean enabled = true;
	private String profileUrl;
	private String profileImageUrl;
	private List<Role> roles = new ArrayList<>();

	@Override
	public Identity getCustomerId() {
		return customerId;
	}

	public String getCoverUrl() {
		return Optional.of(cover)
				.map(Cover::getSource)
				.orElse(null);
	}

	public void setCoverUrl(String coverUrl) {
		if(cover == null) {
			cover = new Cover();
		}

		cover.setSource(coverUrl);
	}

	@Override
	public void addRole(String role) {
		this.roles.add(new Role(role));
	}

	@Getter
	@Setter
	public static class Cover {
		private String id;
		private String source;
	}
}
