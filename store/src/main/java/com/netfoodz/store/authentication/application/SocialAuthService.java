package com.netfoodz.store.authentication.application;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.authentication.application.exception.LoginFailedException;
import com.netfoodz.store.authentication.application.exception.UserNotEnabledException;
import com.netfoodz.store.authentication.application.exception.WebRequestException;
import com.netfoodz.store.authentication.domain.model.FacebookUser;
import com.netfoodz.store.authentication.domain.model.SocialUser;
import com.netfoodz.store.authentication.domain.repository.SocialAuthRepository;
import com.netfoodz.store.authentication.domain.repository.SocialUserRepository;
import com.netfoodz.store.customer.application.CustomerApplicationService;
import com.netfoodz.store.customer.domain.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SocialAuthService {

	private SessionService sessionService;
	private FacebookOAuth2Service facebook;
	private SocialUserRepository socialUserRepository;
	private SocialAuthRepository socialAuthRepository;
	private CustomerApplicationService customerService;

	@Autowired
	public SocialAuthService(SessionService sessionService,
							 FacebookOAuth2Service facebook,
							 SocialUserRepository socialUserRepository,
							 SocialAuthRepository socialAuthRepository,
							 CustomerApplicationService customerService) {
		this.sessionService = sessionService;
		this.facebook = facebook;
		this.socialUserRepository = socialUserRepository;
		this.socialAuthRepository = socialAuthRepository;
		this.customerService = customerService;
	}

	public boolean authenticateWithFacebook(String userId, String accessToken) throws WebRequestException {
		String appId = socialAuthRepository.findFacebookAppId();
		String appSecret = socialAuthRepository.findFacebookAppSecret();
		SocialUser user = socialUserRepository.findFacebookUser(userId);

		if(user != null && !user.isEnabled()) {
			throw new UserNotEnabledException();
		}

		FacebookUser facebookUser;
		try {
			facebookUser = facebook.login(userId, accessToken, appId, appSecret);
		} catch (IOException | LoginFailedException e) {
			throw new WebRequestException("Error on request to login facebook user", e);
		}

		if (facebookUser == null)
			return false;

		if (user == null) {
			facebookUser.addRole("ROLE_USER");
			Identity id = socialUserRepository.save(facebookUser);
			user = facebookUser;
			Customer customer = customerService.createCustomer(id, facebookUser.getName());
			facebookUser.setCustomerId(customer.getId());
		}

		sessionService.login(user);

		return true;
	}
}