package com.netfoodz.store.authentication.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode
@Data
public class Role {

	@NonNull
	private String name;
}
