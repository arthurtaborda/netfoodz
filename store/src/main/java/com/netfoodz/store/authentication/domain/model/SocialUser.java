package com.netfoodz.store.authentication.domain.model;


public interface SocialUser extends User {

	String getSocialAccountId();
	String getCoverUrl();
	String getProfileUrl();
	String getProfileImageUrl();
}
