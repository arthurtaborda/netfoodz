package com.netfoodz.store.authentication.domain.model;

import com.netfoodz.common.domain.model.EmailAddress;
import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;

import java.util.List;

public interface User extends Entity {

	Identity getCustomerId();

	boolean isEnabled();
	String getName();
	EmailAddress getEmail();
	List<Role> getRoles();

	void addRole(String role);
}
