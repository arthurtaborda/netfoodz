package com.netfoodz.store.authentication.application.exception;

public class UserNotEnabledException extends RuntimeException {
	public UserNotEnabledException() {
	}

	public UserNotEnabledException(String s) {
		super(s);
	}

	public UserNotEnabledException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public UserNotEnabledException(Throwable throwable) {
		super(throwable);
	}

	public UserNotEnabledException(String s, Throwable throwable, boolean b, boolean b1) {
		super(s, throwable, b, b1);
	}
}
