package com.netfoodz.store.authentication.infrastructure.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQRoleDao;
import com.netfoodz.jooq.tables.pojos.JQRole;
import com.netfoodz.store.authentication.domain.model.Role;
import com.netfoodz.store.authentication.domain.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Profile("jooq")
@Repository
public class JQRoleRepository implements RoleRepository {

	private JQRoleDao dao;

	@Autowired
	public JQRoleRepository(JQRoleDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Role> findByUserId(Identity userId) {
		List<JQRole> dbRoles = dao.fetchByUserId(userId.toString());
		return dbRoles.stream()
				.map(r -> new Role(r.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public void save(Identity userId, Role role) {
		dao.insert(new JQRole(role.getName(), userId.toString()));
	}
}
