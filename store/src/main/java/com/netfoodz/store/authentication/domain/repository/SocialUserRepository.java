package com.netfoodz.store.authentication.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.authentication.domain.model.FacebookUser;
import com.netfoodz.store.authentication.domain.model.SocialUser;

public interface SocialUserRepository {

	FacebookUser findFacebookUser(String userId);

	Identity save(SocialUser socialUser);
}
