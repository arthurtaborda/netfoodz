package com.netfoodz.store.authentication.infrastructure.jooq;

import com.netfoodz.common.domain.model.EmailAddress;
import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQBasicuserDao;
import com.netfoodz.jooq.tables.pojos.JQBasicuser;
import com.netfoodz.store.authentication.domain.model.BasicUser;
import com.netfoodz.store.authentication.domain.repository.BasicUserRepository;
import com.netfoodz.store.authentication.domain.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;

@Profile("jooq")
@Repository
public class JQBasicUserRepository implements BasicUserRepository {

	private JQBasicuserDao dao;
	private RoleRepository roleRepository;

	@Autowired
	public JQBasicUserRepository(JQBasicuserDao dao, RoleRepository roleRepository) {
		this.dao = dao;
		this.roleRepository = roleRepository;
	}

	@PostConstruct
	public void init() {
		long count = dao.count();

		if (count == 0) {
			JQBasicuser user = new JQBasicuser();
			user.setId(Identity.generate());
			user.setName("NetFoodz User");
			user.setEmail(new EmailAddress("user@netfoodz.com"));
			user.setEncryptedPassword("$2a$10$v35XUkr32oCWu8ZZ41/Eh.nfqqLq1vr.MJQOLTNoDk9bwe1SGQbPO");
			dao.insert(user);
		}
	}

	@Override
	public BasicUser findByEmail(EmailAddress emailAddress) {
		JQBasicuser dbUser = dao.fetchOneByEmail(emailAddress);
		BasicUser user = new BasicUser(emailAddress, dbUser.getEncryptedPassword());
		user.setEnabled(dbUser.getEnabled() == 1);
		user.setName(dbUser.getName());
		user.setRoles(roleRepository.findByUserId(dbUser.getId()));
		return null;
	}
}
