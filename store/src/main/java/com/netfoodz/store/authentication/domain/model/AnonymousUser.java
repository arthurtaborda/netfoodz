package com.netfoodz.store.authentication.domain.model;

import com.netfoodz.common.domain.model.EmailAddress;
import com.netfoodz.common.domain.model.Identity;

import java.util.ArrayList;
import java.util.List;

public class AnonymousUser implements User {

	private final List<Role> roles;

	public AnonymousUser() {
		roles = new ArrayList<>();
		roles.add(new Role("ROLE_ANONYMOUS"));
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Identity getId() {
		return null;
	}

	@Override
	public Identity getCustomerId() {
		return null;
	}

	@Override
	public String getName() {
		return "anonymous";
	}

	@Override
	public EmailAddress getEmail() {
		return null;

	}

	@Override
	public List<Role> getRoles() {
		return roles;
	}

	@Override
	public void addRole(String role) {
		throw new UnsupportedOperationException("Cannot add roles for anonymous user");
	}
}
