package com.netfoodz.store.authentication.infrastructure.jooq;

import com.google.common.collect.Lists;
import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQSocialuserDao;
import com.netfoodz.jooq.tables.pojos.JQSocialuser;
import com.netfoodz.store.authentication.domain.model.FacebookUser;
import com.netfoodz.store.authentication.domain.model.SocialUser;
import com.netfoodz.store.authentication.domain.repository.RoleRepository;
import com.netfoodz.store.authentication.domain.repository.SocialUserRepository;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

import static com.netfoodz.jooq.tables.TBLCustomer.CUSTOMER;
import static com.netfoodz.jooq.tables.TBLSocialuser.SOCIALUSER;


@Profile("jooq")
@Repository
public class JQSocialUserRepository implements SocialUserRepository {

	private DSLContext dsl;
	private JQSocialuserDao dao;
	private RoleRepository roleRepository;

	@Autowired
	public JQSocialUserRepository(DSLContext dsl, JQSocialuserDao dao, RoleRepository roleRepository) {
		this.dsl = dsl;
		this.dao = dao;
		this.roleRepository = roleRepository;
	}

	@Override
	public FacebookUser findFacebookUser(String accountId) {
		Assert.notNull(accountId, "User socialAccountId must not be null");

		List<Field<?>> fields = Lists.newArrayList(SOCIALUSER.fields());
		fields.add(CUSTOMER.ID.as("customerId"));
		fields.add(SOCIALUSER.ACCOUNT_ID.as("socialAccountId"));
		FacebookUser result = dsl.select(fields)
				.from(SOCIALUSER)
				.join(CUSTOMER).on(CUSTOMER.USER_ID.eq(SOCIALUSER.ID.cast(String.class)))
				.where(SOCIALUSER.PROVIDER.eq("facebook"))
				.and(SOCIALUSER.ACCOUNT_ID.eq(accountId))
				.fetchOneInto(FacebookUser.class);

		return result;
	}

	@Override
	public Identity save(SocialUser socialUser) {
		Assert.notNull(socialUser, "User must not be null");

		JQSocialuser pojo = new JQSocialuser();
		Identity id = Identity.generate();
		pojo.setId(id);
		pojo.setAccountId(socialUser.getSocialAccountId());
		pojo.setName(socialUser.getName());
		pojo.setCoverUrl(socialUser.getCoverUrl());
		pojo.setEmail(socialUser.getEmail());
		pojo.setEnabled(socialUser.isEnabled() ? Byte.valueOf("1") : Byte.valueOf("0"));
		pojo.setProfileUrl(socialUser.getProfileUrl());
		pojo.setProfileImageUrl(socialUser.getProfileImageUrl());

		if (socialUser instanceof FacebookUser) {
			pojo.setProvider("facebook");
		}

		dao.insert(pojo);

		if (socialUser.getRoles() != null) {
			socialUser.getRoles()
					.stream()
					.forEach(r -> roleRepository.save(id, r));
		}

		return id;
	}
}
