package com.netfoodz.store.authentication.domain.model;

import com.netfoodz.common.domain.model.EmailAddress;
import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
@Data
public class BasicUser implements Entity, User {

	private Identity id;
	private String name;
	private String customerId;
	private boolean enabled = true;
	@NonNull
	private EmailAddress email;
	@NonNull
	private String encryptedPassword;
	private List<Role> roles = new ArrayList<>();

	@Override
	public void addRole(String role) {
		this.roles.add(new Role(role));
	}

	@Override
	public Identity getCustomerId() {
		return Identity.of(customerId);
	}
}
