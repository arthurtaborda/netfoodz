package com.netfoodz.store.authentication.domain.repository;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.store.authentication.domain.model.Role;

import java.util.List;

public interface RoleRepository {

	List<Role> findByUserId(Identity userId);

	void save(Identity userId, Role role);
}
