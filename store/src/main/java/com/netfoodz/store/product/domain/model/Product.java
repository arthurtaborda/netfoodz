package com.netfoodz.store.product.domain.model;

import com.netfoodz.common.domain.model.Entity;
import com.netfoodz.common.domain.model.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.math.BigDecimal;

@EqualsAndHashCode
@Data
public class Product implements Entity {

	private Identity id;

	@NonNull
	private String name;
	private BigDecimal price;
	private String description;
	private boolean inCatalog;
}
