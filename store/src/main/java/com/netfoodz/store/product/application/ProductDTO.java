package com.netfoodz.store.product.application;

public class ProductDTO {

	public String id;
	public String name;
	public double price;
	public String description;
}
