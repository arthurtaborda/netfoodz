package com.netfoodz.store.product.infrastructure.repository.jooq;

import com.netfoodz.common.domain.model.Identity;
import com.netfoodz.jooq.tables.daos.JQProductDao;
import com.netfoodz.jooq.tables.pojos.JQProduct;
import com.netfoodz.store.product.domain.model.Product;
import com.netfoodz.store.product.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Profile("jooq")
@Repository
public class JQProductRepository implements ProductRepository {

	private JQProductDao dao;

	@Autowired
	public JQProductRepository(JQProductDao dao) {
		this.dao = dao;
	}

	@PostConstruct
	public void init() {
		long count = dao.count();

		if (count == 0) {
			JQProduct coca = new JQProduct();
			coca.setId(Identity.generate());
			coca.setName("Coca-Cola");
			coca.setInCatalog((byte) 1);
			coca.setPrice((double) 3);
			coca.setDescription("A delicious coca-cola");

			JQProduct sunday = new JQProduct();
			sunday.setId(Identity.generate());
			sunday.setName("Sunday");
			sunday.setInCatalog((byte) 0);
			sunday.setPrice((double) 5);
			sunday.setDescription("A delicious sunday");

			JQProduct burguer = new JQProduct();
			burguer.setId(Identity.generate());
			burguer.setName("Burguer");
			burguer.setInCatalog((byte) 0);
			burguer.setPrice((double) 10);
			burguer.setDescription("A delicious burguer");

			dao.insert(coca);
			dao.insert(sunday);
			dao.insert(burguer);
		}
	}

	@Override
	public Product findById(Identity productId) {
		JQProduct dbProduct = dao.fetchOneById(productId);
		return convert(dbProduct);
	}

	private Product convert(JQProduct dbProduct) {
		Product product = new Product(dbProduct.getName());
		product.setId(dbProduct.getId());
		product.setPrice(BigDecimal.valueOf(dbProduct.getPrice()));
		product.setDescription(dbProduct.getDescription());
		product.setInCatalog(dbProduct.getInCatalog() == 1);
		return product;
	}

	@Override
	public List<Product> findInCatalog() {
		List<JQProduct> dbProduct = dao.fetchByInCatalog((byte) 1);
		return dbProduct.stream().map(this::convert).collect(Collectors.toList());
	}
}
