package com.netfoodz.store.mobile.domain.model.stub;

import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import com.netfoodz.store.mobile.domain.model.NotificationRequest;
import com.netfoodz.store.mobile.domain.model.NotificationResult;
import com.netfoodz.store.mobile.domain.service.NotificationClient;

import java.io.IOException;
import java.util.Map;

public class NotificationClientError implements NotificationClient {

	@Override
	public Map<String, NotificationResult> getSuccessfulNotifications() {
		return null;
	}

	@Override
	public Map<String, NotificationResult> getFailedNotifications() {
		return null; //must not enter here
	}

	@Override
	public void send(NotificationRequest notification, String[] devices) throws IOException {
		throw new IOException();
	}

	@Override
	public MobilePlatform getPlatform() {
		return MobilePlatform.ANDROID;
	}
}
