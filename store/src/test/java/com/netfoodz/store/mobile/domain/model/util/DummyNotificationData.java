package com.netfoodz.store.mobile.domain.model.util;

import com.netfoodz.store.mobile.domain.model.NotificationRequest;

public class DummyNotificationData {
	public DummyNotificationData(Integer numOfDevices) {
		devices = new String[numOfDevices];
		for (int i = 0; i < numOfDevices; i++) {
			devices[i] = "device" + i;
		}

		request = new NotificationRequest("Order Done", "Your order is ready", "Go get it", "ab4c4b6");
	}

	public String[] devices;
	public NotificationRequest request;
}
