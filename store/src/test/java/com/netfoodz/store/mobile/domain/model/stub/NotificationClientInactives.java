package com.netfoodz.store.mobile.domain.model.stub;

import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import com.netfoodz.store.mobile.domain.model.NotificationRequest;
import com.netfoodz.store.mobile.domain.model.NotificationResult;
import com.netfoodz.store.mobile.domain.model.NotificationStatus;
import com.netfoodz.store.mobile.domain.service.NotificationClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NotificationClientInactives implements NotificationClient {

	@Override
	public Map<String, NotificationResult> getSuccessfulNotifications() {
		return new HashMap<>();
	}

	@Override
	public Map<String, NotificationResult> getFailedNotifications() {
		Map<String, NotificationResult> deactivated = new HashMap<>();
		NotificationResult value = new NotificationResult();
		NotificationResult value2 = new NotificationResult();
		value.setStatus(NotificationStatus.SERVER_ERROR);
		value2.setStatus(NotificationStatus.SERVER_ERROR);
		value2.setDeviceDeactivated(true);
		deactivated.put("device1", value);
		deactivated.put("device2", value2);
		return deactivated;
	}

	@Override
	public void send(NotificationRequest notification, String[] devices) throws IOException {
	}

	@Override
	public MobilePlatform getPlatform() {
		return MobilePlatform.ANDROID;
	}
}
