package com.netfoodz.store.mobile.domain.model.stub;

import com.netfoodz.store.mobile.domain.model.MobilePlatform;
import com.netfoodz.store.mobile.domain.model.NotificationRequest;
import com.netfoodz.store.mobile.domain.model.NotificationResult;
import com.netfoodz.store.mobile.domain.service.NotificationClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NotificationClientSuccess implements NotificationClient {

	@Override
	public Map<String, NotificationResult> getSuccessfulNotifications() {
		return new HashMap<>();
	}

	@Override
	public Map<String, NotificationResult> getFailedNotifications() {
		return new HashMap<>();
	}

	@Override
	public void send(NotificationRequest notification, String[] devices) throws IOException {
	}

	@Override
	public MobilePlatform getPlatform() {
		return MobilePlatform.ANDROID;
	}
}
