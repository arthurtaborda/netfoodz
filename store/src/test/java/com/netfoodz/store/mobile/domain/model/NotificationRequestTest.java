package com.netfoodz.store.mobile.domain.model;

import com.netfoodz.store.mobile.domain.model.stub.NotificationClientError;
import com.netfoodz.store.mobile.domain.model.stub.NotificationClientInactives;
import com.netfoodz.store.mobile.domain.model.stub.NotificationClientSuccess;
import com.netfoodz.store.mobile.domain.model.util.DummyNotificationData;
import com.netfoodz.store.mobile.domain.service.NotificationClient;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class NotificationRequestTest {

	public void testSend(NotificationClient client, DummyNotificationData dummy, NotificationStatus[] types) {
		List<NotificationSent> notifications = dummy.request.sendToDevices(client, 1000, dummy.devices);
		Set<String> devices = notifications.stream().map(n -> n.getDeviceId().toString()).collect(Collectors.toSet());
		List<NotificationStatus> statuses = notifications.stream().map(NotificationSent::getStatus).collect(Collectors.toList());

		assertEquals(notifications.size(), dummy.devices.length);
		assertThat(devices, contains(dummy.devices));
		assertThat(statuses, contains(types));
	}

	@Test
	public void testSuccessClientHasSuccess() throws Exception {
		NotificationClient client = new NotificationClientSuccess();

		DummyNotificationData dummy = new DummyNotificationData(2);

		NotificationStatus[] types = {
				NotificationStatus.SUCCESS,
				NotificationStatus.SUCCESS
		};

		testSend(client, dummy, types);
	}

	@Test
	public void testServerErrorWhenDeviceInactive() throws Exception {
		NotificationClient client = new NotificationClientInactives();

		DummyNotificationData dummy = new DummyNotificationData(3);

		NotificationStatus[] types = {
				NotificationStatus.SUCCESS,
				NotificationStatus.SERVER_ERROR,
				NotificationStatus.SERVER_ERROR
		};

		testSend(client, dummy, types);
	}

	@Test
	public void testSendErrorWhenBadThingsHappen() throws Exception {
		NotificationClient client = new NotificationClientError();

		DummyNotificationData dummy = new DummyNotificationData(2);

		NotificationStatus[] types = {
				NotificationStatus.SEND_ERROR,
				NotificationStatus.SEND_ERROR
		};

		testSend(client, dummy, types);
	}

	@Test
	public void testSuccess() throws Exception {
		NotificationClientSuccess client = new NotificationClientSuccess();

		DummyNotificationData dummy = new DummyNotificationData(2);

		Map<String, NotificationResult> notifications = dummy.request.sendMessageToDevices(client, dummy.devices);

		String[] successTypes = {NotificationStatus.SUCCESS.toString(), NotificationStatus.SUCCESS.toString()};
		List<String> statuses = notifications.values().stream()
				.map((notificationResult) -> notificationResult.getStatus().toString()).collect(Collectors.toList());

		assertEquals(notifications.size(), dummy.devices.length);
		assertThat(notifications.keySet(), contains(dummy.devices));
		assertThat(statuses, contains(successTypes));
	}

	@Test(expected = IOException.class)
	public void testServerError() throws Exception {
		NotificationClientError client = new NotificationClientError();

		DummyNotificationData dummy = new DummyNotificationData(2);

		dummy.request.sendMessageToDevices(client, dummy.devices);
	}

	@Test
	public void testSendError() throws IOException {
		NotificationClientInactives client = new NotificationClientInactives();

		DummyNotificationData dummy = new DummyNotificationData(3);

		Map<String, NotificationResult> notifications = dummy.request.sendMessageToDevices(client, dummy.devices);

		NotificationResult first = notifications.get(dummy.devices[0]);
		NotificationResult second = notifications.get(dummy.devices[1]);
		NotificationResult third = notifications.get(dummy.devices[2]);

		assertEquals(notifications.size(), dummy.devices.length);
		assertThat(notifications.keySet(), contains(dummy.devices));
		assertEquals(first.getStatus(), NotificationStatus.SUCCESS);
		assertEquals(second.getStatus(), NotificationStatus.SERVER_ERROR);
		assertEquals(third.getStatus(), NotificationStatus.SERVER_ERROR);
		assertTrue(third.isDeviceDeactivated());
	}

}