-- MySQL dump 10.16  Distrib 10.1.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: netfoodz_dev
-- ------------------------------------------------------
-- Server version	10.1.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `basicuser`
--

DROP TABLE IF EXISTS `basicuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basicuser` (
	`id` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	`enabled` tinyint(1) NOT NULL DEFAULT '1',
	`encrypted_password` varchar(255) NOT NULL,
	`name` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `UK_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
	`id` varchar(255) NOT NULL,
	`customer_id` varchar(255) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `FK_customer` (`customer_id`),
	CONSTRAINT `FK_cart_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cartitem`
--

DROP TABLE IF EXISTS `cartitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartitem` (
	`id` varchar(255) NOT NULL,
	`added_at` datetime DEFAULT NOW(),
	`cart_id` varchar(255) NOT NULL,
	`product_id` varchar(255) NOT NULL,
	`price` DOUBLE NOT NULL,
	`quantity` INT NOT NULL DEFAULT 0,
	`observation` LONGTEXT DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `FK_cartitem_cart` (`cart_id`),
	KEY `FK_cartitem_product` (`product_id`),
	CONSTRAINT `FK_cartitem_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
	CONSTRAINT `FK_cartitem_cart` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
	`id` varchar(255) NOT NULL,
	`name` varchar(255) DEFAULT NULL,
	`user_id` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobiledevice`
--

DROP TABLE IF EXISTS `mobiledevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobiledevice` (
	`id` varchar(255) NOT NULL,
	`active` tinyint(1) NOT NULL DEFAULT '1',
	`lat` double DEFAULT NULL,
	`lng` double DEFAULT NULL,
	`platform` varchar(255) DEFAULT NULL,
	`current_user_id` varchar(255) DEFAULT NULL,
	`previous_user_id` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
	`id` varchar(255) NOT NULL,
	`description` varchar(255) DEFAULT NULL,
	`in_catalog` tinyint(1) NOT NULL DEFAULT '1',
	`name` varchar(255) NOT NULL,
	`price` double NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
	`name` varchar(255) NOT NULL,
	`user_id` varchar(255) NOT NULL,
	PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `socialuser`
--

DROP TABLE IF EXISTS `socialuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialuser` (
	`id` varchar(255) NOT NULL,
	`account_id` varchar(255) NOT NULL,
	`cover_url` varchar(255) DEFAULT NULL,
	`email` varchar(255) NOT NULL,
	`enabled` tinyint(1) NOT NULL DEFAULT '1',
	`name` varchar(255) DEFAULT NULL,
	`profile_image_url` varchar(255) DEFAULT NULL,
	`profile_url` varchar(255) DEFAULT NULL,
	`provider` varchar(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `UK_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
	`id` varchar(255) NOT NULL,
	`state` VARCHAR(255) NOT NULL DEFAULT 'WAITING_FOR_PAYMENT',
	`created_at` DATETIME DEFAULT NOW(),
	`customer_id` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `FK_order_customer` (`customer_id`),
	CONSTRAINT `FK_order_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderline`
--

DROP TABLE IF EXISTS `orderline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderline` (
	`id` varchar(255) NOT NULL,
	`order_id` varchar(255) NOT NULL,
	`product_id` varchar(255) NOT NULL,
	`price` DOUBLE NOT NULL,
	`quantity` INT NOT NULL DEFAULT 0,
	`observation` LONGTEXT DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `FK_orderitem_order` (`order_id`),
	KEY `FK_orderitem_product` (`product_id`),
	CONSTRAINT `FK_orderitem_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
	CONSTRAINT `FK_orderitem_order` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
	`id` varchar(255) NOT NULL,
	`device_id` varchar(255) NOT NULL,
	`notification_id` varchar(255) DEFAULT NULL,
	`notification_group_id` varchar(255) DEFAULT NULL,
	`error_message` varchar(255) DEFAULT NULL,
	`device_deactivated` TINYINT(1) NOT NULL DEFAULT '0',
	`error_codename` VARCHAR(255) DEFAULT NULL,
	`status` VARCHAR(255) NOT NULL DEFAULT 'SUCCESS',
	`platform` VARCHAR(255) NOT NULL DEFAULT 'ANDROID',
	`stack_trace` LONGTEXT DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `FK_notification_device` (`device_id`),
	CONSTRAINT `FK_notification_device` FOREIGN KEY (`device_id`) REFERENCES `mobiledevice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-15 10:50:23
